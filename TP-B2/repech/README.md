# Sujet Rattrapage B2

# I. Setup

## 1. Emby server

> A faire sur 🖥️ `web.peche.linux`.

🌞 **Mettez en place le serveur Emby en suivant la doc officielle**

```bash
[ced@web ~]$ sudo yum install https://github.com/MediaBrowser/Emby.Releases/releases/download/4.7.11.0/emby-server-rpm_4.7.11.0_x86_64.rpm
[...]
Installed:
  emby-server-4.7.11.0-1.x86_64
```

🌞 **Une fois en place, mettez en évidence :**

```bash
[ced@web ~]$ sudo systemctl status emby-server
● emby-server.service - Emby Server is a personal media server with apps on just about every device
     Loaded: loaded (/usr/lib/systemd/system/emby-server.service; enabled; vendor preset: disabled)
     Active: active (running) since Wed 2023-02-08 14:38:06 CET; 4min 25s ago


[ced@web ~]$ journalctl -xe | grep emby
Feb 08 14:34:24 web.peche.linux sudo[1313]:      ced : TTY=pts/0 ; PWD=/home/ced ; USER=root ; COMMAND=/bin/yum install https://github.com/MediaBrowser/Emby.Releases/releases/download/4.7.11.0/emby-server-rpm_4.7.11.0_x86_64.rpm
Feb 08 14:38:05 web.peche.linux groupadd[1329]: group added to /etc/group: name=emby, GID=991
Feb 08 14:38:05 web.peche.linux groupadd[1329]: group added to /etc/gshadow: name=emby
Feb 08 14:38:05 web.peche.linux groupadd[1329]: new group: name=emby, GID=991
Feb 08 14:38:05 web.peche.linux useradd[1338]: new user: name=emby, UID=991, GID=991, home=/var/lib/emby, shell=/usr/sbin/nologin, from=none
Feb 08 14:38:06 web.peche.linux usermod[1348]: add 'emby' to group 'video'
Feb 08 14:38:06 web.peche.linux usermod[1348]: add 'emby' to shadow group 'video'
Feb 08 14:38:06 web.peche.linux usermod[1356]: add 'emby' to group 'render'
Feb 08 14:38:06 web.peche.linux usermod[1356]: add 'emby' to shadow group 'render'
░░ Subject: A start job for unit emby-server.service has finished successfully
[...]


[ced@web ~]$ ps -ef | grep emby
emby        1542       1  5 14:47 ?        00:00:02 /opt/emby-server/system/EmbyServer -programdata /var/lib/emby -ffdetect /opt/emby-server/bin/ffdetect -ffmpeg /opt/emby-server/bin/ffmpeg -ffprobe /opt/emby-server/bin/ffprobe -restartexitcode 3 -updatepackage emby-server-rpm_{version}_x86_64.rpm
ced         1575    1180  0 14:47 pts/0    00:00:00 grep --color=auto emby

[ced@web ~]$ sudo ss -tunlp | grep Emby
udp   UNCONN 0      0          127.0.0.1:59468      0.0.0.0:*    users:(("EmbyServer",pid=1542,fd=226))
udp   UNCONN 0      0            0.0.0.0:34428      0.0.0.0:*    users:(("EmbyServer",pid=1542,fd=224))
udp   UNCONN 0      0          10.0.2.15:43189      0.0.0.0:*    users:(("EmbyServer",pid=1542,fd=225))
udp   UNCONN 0      0            0.0.0.0:1900       0.0.0.0:*    users:(("EmbyServer",pid=1542,fd=223))
udp   UNCONN 0      0                  *:7359             *:*    users:(("EmbyServer",pid=1542,fd=219))
tcp   LISTEN 0      512                *:8096             *:*    users:(("EmbyServer",pid=1542,fd=193))

[ced@web ~]$ ps -fu emby
UID          PID    PPID  C STIME TTY          TIME CMD
emby        1542       1  0 14:47 ?        00:00:02 /opt/emby-server/system/EmbyServer -programdata /var/lib/emby -ffdetect /opt/emby-server/bin/ffdetect
```

🌞 **Accédez à l'interface Web**

```bash
[ced@web ~]$ sudo firewall-cmd --add-port=8096/tcp --permanent
[ced@web ~]$ sudo firewall-cmd --reload
```
acces sur navigateur ok

🌞 **Configurer un répertoire pour accueillir vos médias**
```bash
[ced@web ~]$ sudo mkdir /srv/media

[ced@web ~]$ sudo chown emby /srv/media/

PS C:\Users\vidal\Downloads> scp RickRoll.mp3 ced@10.108.1.11:/home/ced
RickRoll.mp3                                                            100% 4972KB  74.5MB/s   00:00

[ced@web ~]$ ls /srv/media/
RickRoll.mp3
``` 


- donnez lui les permissions les plus restrictives possibles, le minimumm pour que tout fonctionne correctement



> Le stream fonctionne


## 2. Reverse proxy


🌞 **Mettez en place NGINX**

```bash
[ced@proxy ~]$ sudo dnf install nginx

Installed:
  nginx-1:1.20.1-13.el9.x86_64                          nginx-core-1:1.20.1-13.el9.x86_64
  nginx-filesystem-1:1.20.1-13.el9.noarch               rocky-logos-httpd-90.14-1.el9.noarch

[ced@proxy ~]$ sudo systemctl enable nginx
Created symlink /etc/systemd/system/multi-user.target.wants/nginx.service → /usr/lib/systemd/system/nginx.service.

[ced@proxy ~]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
     Loaded: loaded (/usr/lib/systemd/system/nginx.service; enabled; vendor preset: disabled)
     Active: active (running) since Wed 2023-02-08 17:36:01 CET; 2s ago

[ced@proxy ~]$ sudo ss -tulnp | grep nginx
tcp   LISTEN 0      511          0.0.0.0:80        0.0.0.0:*    users:(("nginx",pid=1560,fd=6),("nginx",pid=1559,fd=6))
tcp   LISTEN 0      511             [::]:80           [::]:*    users:(("nginx",pid=1560,fd=7),("nginx",pid=1559,fd=7))

[ced@proxy ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[ced@proxy ~]$ sudo firewall-cmd --add-port=8096/tcp --permanent
success
[ced@proxy ~]$ sudo firewall-cmd --reload
success
[ced@proxy ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 80/tcp 8096/tcp
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

[ced@proxy ~]$ ps -ef | grep nginx
root        1559       1  0 17:36 ?        00:00:00 nginx: master process /usr/sbin/nginx
nginx       1560    1559  0 17:36 ?        00:00:00 nginx: worker process

[ced@proxy ~]$ sudo vim /etc/nginx/con.d/nginx.conf
[ced@proxy ~]$ sudo systemctl restart nginx
```

Accès proxy ok

```bash
[ced@proxy ~]$ mkdir ~/ssl-certificates

[ced@proxy ~]$ openssl req -new -newkey rsa:2048 -nodes -keyout server.key -out server.csr

[ced@proxy ~]$ openssl x509 -req -days 365 -in server.csr -signkey server.key -out server.crt

[ced@proxy ~]$ mv ssl-certificates/ /etc/nginx/

[ced@proxy ~]$ sudo cat /etc/nginx/conf.d/nginx.conf

[sudo] password for ced:
server {
    # On indique le nom que client va saisir pour accéder au service
    # Pas d'erreur ici, c'est bien le nom de web, et pas de proxy qu'on veut ici !
    server_name web.peche.linux;

    # Port d'écoute de NGINX
    listen 443 ssl;

    ssl_certificate /etc/nginx/ssl-certificates/server.crt;
    ssl_certificate_key /etc/nginx/ssl-certificates/server.key;

    location / {
        # On définit des headers HTTP pour que le proxying se passe bien
        proxy_set_header  Host $host;
        proxy_set_header  X-Real-IP $remote_addr;
        proxy_set_header  X-Forwarded-Proto https;
        proxy_set_header  X-Forwarded-Host $remote_addr;
        proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;

        # On définit la cible du proxying
        proxy_pass http://10.108.1.11:8096;
    }
}
```
la connection en hettp avec l'ip fonctionne




  - utilisez le nom `emby.peche.linux` pour le certificat
  - ce sera aussi le nom à utiliser pour visiter l'application
- vous accéderez donc par la suite à l'interface Web en tapant `https://emby.peche.linux` dans votre navigateur

> **Ne passez pas à la suite** tant que vous ne pouvez pas accéder à l'application Web en tapant `https://emby.peche.linux` dans votre navigateur sur votre PC.

### B. Secu

> A faire sur 🖥️ `web.peche.linux`.

🌞 **Ajoutez une règle firewall**

- l'interface Web doit être injoignable si on utilise l'IP de `web.peche.linux`
- on peut uniquement la joindre en passant par `proxy.peche.linux`
- vous devez donc ajouter une règle firewall pour bloquer le trafic qui vient d'une autre machine que `proxy.peche.linux` sur le port de l'interface Web

## 3. Backup

### A. Serveur NFS

> A faire sur 🖥️ `backup.peche.linux`.

Sur cette dernière machine, vous mettrez en place un serveur NFS.

🌞 **Mettez en place un serveur NFS**

- création d'un répertoire `/backups`
- création d'un répertoire `/backups/music/`
- référez-vous au [à la partie II du module 4 du TP3](../3/4-backup/README.md#ii-nfs) pour + de détails
  - vous devez partager à la machine `web.peche.linux` le dossier `/backup/music/`

### B. Client NFS

> A faire sur 🖥️ `web.peche.linux`.

🌞 **Mettez en place un client NFS**

- création d'un répertoire `/backup`
- référez-vous au [à la partie II du module 4 du TP3](../3/4-backup/README.md#ii-nfs) pour + de détails
  - vous devez monter le dossier `/backups/music/` qui se trouve sur `backup.peche.linux`
  - et le rendre accessible sur le dossier `/backup`

### C. Script de backup

> A faire sur votre machine (oui vous arrêtez de coder avec `vim` ou `nano` si vous maîtriser quedal et utilisez VSCode ou votre IDE préféré, comme d'hab). Le script doit s'exécuter sur `web.peche.linux`.

🌞 **Ecrivez un script de backup**

- langage `bash`
- il sera nommé `backup.sh` et stocké dans `/srv`
- vous utiliserez [Borg](https://www.borgbackup.org/) pour créer les sauvegardes

🌞 **Créez un *service* `backup.service`**

- il permet d'exécuter votre script de backup
- il s'exécute sous l'identité d'un utilisateur dédié : `backup` (nécessaire de le créer à la main au préalable donc)
- référez-vous au [TP1](../1/README.md) pour la création de *service*

🌞 **Créez un *timer* `backup.timer`**

- il exécute `backup.service` à intervalles réguliers
- plus précisément : `backup.service` est automatiquement lancé tous les matins à 07h00
