# TP2 : Ethernet, IP, et ARP

# 0. Prérequis

Utilisation d'une Vm et du PC

---

# I. Setup IP

🌞 **Mettez en place une configuration réseau fonctionnelle entre les deux machines**


  - les deux IPs choisies:  
  VM: 10.5.1.3/26  
  Pc: 10.5.1.50/26  
  
  - l'adresse de réseau  
  10.5.1.0/26

  - l'adresse de broadcast  
  10.5.1.63/26


- vous renseignerez aussi les commandes utilisées pour définir les adresses IP *via* la ligne de commande

Pc: Via Vbox  
VM:  
```bash
ced@ced-vm:~$ sudo nano /etc/netplan/01-network-manager-all.yaml

network:
  version: 2
  renderer: NetworkManager
  ethernets:
    enp0s8:
      dhcp4: no
      addresses:
        - 10.5.1.2/26  

ced@ced-vm:~$ netplan apply
```

🌞 **Prouvez que la connexion est fonctionnelle entre les deux machines**

- un `ping` suffit !  
Pc:  
```
PS C:\Users\vidal> ping 10.5.1.3

Envoi d’une requête 'Ping'  10.5.1.3 avec 32 octets de données :
Réponse de 10.5.1.3 : octets=32 temps<1ms TTL=64
Réponse de 10.5.1.3 : octets=32 temps<1ms TTL=64
Réponse de 10.5.1.3 : octets=32 temps<1ms TTL=64
Réponse de 10.5.1.3 : octets=32 temps<1ms TTL=64

Statistiques Ping pour 10.5.1.3:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 0ms, Maximum = 0ms, Moyenne = 0ms
```


🌞 **Wireshark it**

- **déterminez, grâce à Wireshark, quel type de paquet ICMP est envoyé par `ping`**

ICMP:  
Ping: Type 8 (echo (ping) request)  
Pong: Type 0 (echo (ping) reply)  


🦈 **PCAP qui contient les paquets ICMP qui vous ont permis d'identifier les types ICMP**

- [PCAP](./ressources/pingwireshark.pcapng)

# II. ARP my bro

🌞 **Check the ARP table**

- utilisez une commande pour afficher votre table ARP  
`PS C:\Users\vidal> arp -a`

- déterminez la MAC de votre binome depuis votre table ARP  
```
Interface : 10.5.1.50 --- 0x14
 Adresse Internet      Adresse physique      Type
 10.5.1.3              08-00-27-e1-7b-04     dynamique
```

- déterminez la MAC de la *gateway* de votre réseau  
```
Interface : 10.5.1.50 --- 0x14
 Adresse Internet      Adresse physique      Type
 192.168.0.1           40-65-a3-e0-cb-d3     dynamique
```

🌞 **Manipuler la table ARP**  

- utilisez une commande pour vider votre table ARP  
    `arp -d`  

- prouvez que ça fonctionne en l'affichant et en constatant les changements

```
Interface : 10.5.1.50 --- 0x14
  Adresse Internet      Adresse physique      Type
  224.0.0.22            01-00-5e-00-00-16     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
```  
Il n'y a plus l'ip de la VM  

- ré-effectuez des pings, et constatez la ré-apparition des données dans la table ARP
```
Interface : 10.5.1.50 --- 0x14
  Adresse Internet      Adresse physique      Type
  10.5.1.3              08-00-27-e1-7b-04     dynamique
  224.0.0.22            01-00-5e-00-00-16     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
```  
L'IP est de nouveau dans la table ARP  

🌞 **Wireshark it**


  - déterminez, pour les deux trames, les adresses source et destination  
  Ping pc depuis VM  
  ```
  Sender MAC address: 0a:00:27:00:14
  Sender IP address: 10.5.1.50
  Target MAC address: 00:00:00_00:00:00
  Target IP address: 10.5.1.3
  ```

  ```
  Sender MAC address: PcsCompu_e1:7b:04 (08:00:e1:7b:04)
  Sender IP address: 10.5.1.3
  Target MAC address: 0a:00:27:00:00:14
  Target IP address: 10.5.1.50
  ```

Send = celui qui fait la requete
Target = destinataire

🦈 **PCAP qui contient les trames ARP**

- [FirstARPPCAP](./ressources/firstpingarp.pcapng)

# II.5 Interlude hackerzz

Le ferais après promis

# III. DHCP you too my brooo


Quand on arrive dans un réseau, notre PC contacte un serveur DHCP, et récupère généralement 3 infos :

- **1.** une IP à utiliser
- **2.** l'adresse IP de la passerelle du réseau
- **3.** l'adresse d'un serveur DNS joignable depuis ce réseau

L'échange DHCP consiste en 4 trames : DORA, que je vous laisse google vous-mêmes : D

🌞 **Wireshark it**

Def d'une IP static pour permettre au DHCP d'attribuer une nouvelle IP  
```
netsh interface ipv4 set address name="Wi-Fi" static 192.168.0.11 255.255.255.0 192.168.0.1  
netsh interface ipv4 set address name="Wi-Fi" DHCP  
```

Trame1 Dicover:  
Src: 0.0.0.0  
Dst: 255.255.255.255  
Requested IP address: 192.168.0.27

Trame2 Offer: 
Src: 192.168.0.1  
Dst: 192.169.0.27   
Passerelle (router): 192.168.0.1  
DNS: 89.2.0.1/89.2.0.2  

Trame3 Request:  
Src: 0.0.0.0
Dst: 255.255.255.255  

Trame4 Ack:  
Src: 192.168.0.1  
Dst: 192.169.0.27


🦈 **PCAP qui contient l'échange DORA**

- [PCAPDORA](./ressources/pcapDORA.pcapng)


# IV. Avant-goût TCP et UDP

TCP et UDP ce sont les deux protocoles qui utilisent des ports. Si on veut accéder à un service, sur un serveur, comme un site web :

- il faut pouvoir joindre en terme d'IP le correspondant
  - on teste que ça fonctionne avec un `ping` généralement
- il faut que le serveur fasse tourner un programme qu'on appelle "service" ou "serveur"
  - le service "écoute" sur un port TCP ou UDP : il attend la connexion d'un client
- le client **connaît par avance** le port TCP ou UDP sur lequel le service écoute
- en utilisant l'IP et le port, il peut se connecter au service en utilisant un moyen adapté :
  - un navigateur web pour un site web
  - un `ncat` pour se connecter à un autre `ncat`
  - et plein d'autres, **de façon générale on parle d'un client, et d'un serveur**

---

🌞 **Wireshark it**

Le port est 443


🦈 **PCAP qui contient un extrait de l'échange qui vous a permis d'identifier les infos**
