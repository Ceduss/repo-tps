# TP4 : TCP, UDP et services réseau


# 0. Prérequis

➜ Pour ce TP, on va se servir de VMs Rocky Linux. 1Go RAM c'est large large. Vous pouvez redescendre la mémoire vidéo aussi.  

➜ Les firewalls de vos VMs doivent **toujours** être actifs (et donc correctement configurés).

➜ **Si vous voyez le p'tit pote 🦈 c'est qu'il y a un PCAP à produire et à mettre dans votre dépôt git de rendu.**

# I. First steps


🌞 **Déterminez, pour ces 5 applications, si c'est du TCP ou de l'UDP**  
🌞 **Demandez l'avis à votre OS** (on va faire les deux en même temps c'est plus cool :p)  



- Applis:  
    1. **Deepl** (petit traducteur très pratique en app, tu surlignes, et tu fais Ctrl+C+C et hop ta la trad qui s'affiche):
    - **IP et Port du Serv:** `172.65.229.194:443`  
    - **Port Local:** `55615`   
    ```powershell
    PS C:\WINDOWS\system32> netstat -b -n -p TCP
    [...]
    TCP    10.33.16.94:55622      172.65.229.194:443     ESTABLISHED
    [DeepL.exe]
    ```
    - [CapurePCAP](./ressources/deepl.pcapng)


    2. Profitons que **Wireshark** fasse une MAJ (en vrai me soulait d'attendre que le dl finisse :3 soo je vais en profiter eheh) !  
    - **IP et Port du Serv:** `159.89.89.188:443`  
    - **Port Local:** `55436`  
    ```powershell
    PS C:\WINDOWS\system32> netstat -b -n -p TCP
    [...]
    TCP    10.33.16.94:55436      159.89.89.188:443      ESTABLISHED
    [Wireshark.exe]
    ```   
    - [CapturePCAP](./ressources/wireshark.pcapng)  

    3. **Discord** ? c'est pas très fun mais on ne se refais pas (pour la peine je vais me mettre dans un salon voc tout seul n_n)  
    - **IP et Port du Serv:** `162.159.134.234:443`  
    - **Port Local:** `55159`  
    ```powershell
    PS C:\WINDOWS\system32> netstat -b -n -p TCP
    [...]
    TCP    10.33.16.94:55159      162.159.134.234:443    ESTABLISHED
    [Discord.exe]
    TCP    10.33.16.94:53617      162.159.133.232:443    ESTABLISHED
    [Discord.exe]
    TCP    10.33.16.94:53943      162.159.129.235:443    ESTABLISHED
    [Discord.exe]  
    TCP    10.33.16.94:56139      162.159.130.232:443    ESTABLISHED
    [Discord.exe]
    ```  
    (tavu sont pas dans l'ordre les IP :3 c'est fais exprès pour titiller l'autisme des gens comme moi :D)  
    - [CaptureDiscord](./ressources/discord.pcapng)


    3. Et pourquoi pas **NMap** 




- avec Wireshark, on va faire les chirurgiens réseau
- déterminez, pour chaque application :
  - IP et port du serveur auquel vous vous connectez
  - le port local que vous ouvrez pour vous connecter

> Dès qu'on se connecte à un serveur, notre PC ouvre un port random. Une fois la connexion TCP ou UDP établie, entre le port de notre PC et le port du serveur qui est en écoute, on parle de tunnel TCP ou de tunnel UDP.


> Aussi, TCP ou UDP ? Comment le client sait ? Il sait parce que le serveur a décidé ce qui était le mieux pour tel ou tel type de trafic (un jeu, une page web, etc.) et que le logiciel client est codé pour utiliser TCP ou UDP en conséquence.

🌞 **Demandez l'avis à votre OS**


  TCP    10.33.16.94:55436      159.89.89.188:443      ESTABLISHED
 [Wireshark.exe]

  TCP    10.33.16.94:55622      172.65.229.194:443     ESTABLISHED
 [DeepL.exe]

- votre OS est responsable de l'ouverture des ports, et de placer un programme en "écoute" sur un port
- il est aussi responsable de l'ouverture d'un port quand une application demande à se connecter à distance vers un serveur
- bref il voit tout quoi
- utilisez la commande adaptée à votre OS pour repérer, dans la liste de toutes les connexions réseau établies, la connexion que vous voyez dans Wireshark, pour chacune des 5 applications

**Il faudra ajouter des options adaptées aux commandes pour y voir clair. Pour rappel, vous cherchez des connexions TCP ou UDP.**

```
# MacOS
$ netstat

# GNU/Linux
$ ss

# Windows
$ netstat
```

🦈🦈🦈🦈🦈 **Bah ouais, captures Wireshark à l'appui évidemment.** Une capture pour chaque application, qui met bien en évidence le trafic en question.

# II. Mise en place

Allumez une VM Linux pour la suite.

## 1. SSH

Connectez-vous en SSH à votre VM.

🌞 **Examinez le trafic dans Wireshark**

- donnez un sens aux infos devant vos yeux, capturez un peu de trafic, et coupez la capture, sélectionnez une trame random et regardez dedans, vous laissez pas brainfuck par Wireshark n_n
- **déterminez si SSH utilise TCP ou UDP**
  - pareil réfléchissez-y deux minutes, logique qu'on utilise pas UDP non ?
- **repérez le *3-Way Handshake* à l'établissement de la connexion**
  - c'est le `SYN` `SYNACK` `ACK`
- **repérez le FIN FINACK à la fin d'une connexion**
- entre le *3-way handshake* et l'échange `FIN`, c'est juste une bouillie de caca chiffré, dans un tunnel TCP

🌞 **Demandez aux OS**

- repérez, avec un commande adaptée, la connexion SSH depuis votre machine
- ET repérez la connexion SSH depuis votre VM

```
# MacOS
$ netstat

# GNU/Linux
$ ss

# Windows
$ netstat
```

🦈 **Je veux une capture clean avec le 3-way handshake, un peu de trafic au milieu et une fin de connexion**

## 2. NFS

Allumez une deuxième VM Linux pour cette partie.

Vous allez installer un serveur NFS. Un serveur NFS c'est juste un programme qui écoute sur un port (comme toujours en fait, oèoèoè) et qui propose aux clients d'accéder à des dossiers à travers le réseau.

Une de vos VMs portera donc le serveur NFS, et l'autre utilisera un dossier à travers le réseau.

🌞 **Mettez en place un petit serveur NFS sur l'une des deux VMs**

- j'vais pas ré-écrire la roue, google it, ou [go ici](https://www.server-world.info/en/note?os=Rocky_Linux_8&p=nfs&f=1)
- partagez un dossier que vous avez créé au préalable dans `/srv`
- vérifiez que vous accédez à ce dossier avec l'autre machine : [le client NFS](https://www.server-world.info/en/note?os=Rocky_Linux_8&p=nfs&f=2)

> Si besoin, comme d'hab, je peux aider à la compréhension, n'hésitez pas à m'appeler.

🌞 **Wireshark it !**

- une fois que c'est en place, utilisez `tcpdump` pour capturer du trafic NFS
- déterminez le port utilisé par le serveur

🌞 **Demandez aux OS**

- repérez, avec un commande adaptée, la connexion NFS sur le client et sur le serveur

```
# GNU/Linux
$ ss
```

🦈 **Et vous me remettez une capture de trafic NFS** la plus complète possible. J'ai pas dit que je voulais le plus de trames possible, mais juste, ce qu'il faut pour avoir un max d'infos sur le trafic

## 3. DNS

🌞 Utilisez une commande pour effectuer une requête DNS depuis une des VMs

- capturez le trafic avec un `tcpdump`
- déterminez le port et l'IP du serveur DNS auquel vous vous connectez

