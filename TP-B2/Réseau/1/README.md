# TP1 - Mise en jambes

**Dans ce TP, on va explorer le réseau de vos clients, vos PC.**

Le terme *réseau* désigne au sens large toutes les fonctionnalités d'un PC permettant de se connecter à d'autres machines.  

De façon simplifiée, on appelle *stack TCP/IP* ou *pile TCP/IP* l'ensemble de logiciels qui permettent d'utiliser et configurer des cartes réseau.  

C'est juste un gros mot savant pour désigner tout ce qui touche de près ou de loin au réseau dans une machine okay ? :)

Lorsque l'on parle de réseau, on désigne souvent par *client* tout équipement qui porte une adresse IP.

Donc vos PCs sont des *clients*, et on va explorer leur *réseau*, c'est à dire, leur *pile TCP/IP*.

# I. Exploration locale en solo

## 1. Affichage d'informations sur la pile TCP/IP locale

### En ligne de commande

En utilisant la ligne de commande (CLI) de votre OS :

**🌞 Affichez les infos des cartes réseau de votre PC**

```
PS C:\Users\vidal>ipconfig /all
[...]
- Carte réseau sans fil Wi-Fi :
    Adresse IPv4. . . . . . . . . . . . . .: 10.33.19.85  
    Adresse physique . . . . . . . . . . . : 02-5F-D3-A4-47-1B  

- Interface Ethernet :  
    J'en ai pô, ultrabook tout ça tu connais :3 ; mais en devinant j'imagine qu'elle doit avoir une adresse MAC mais pas d'IP puisqu'elle est pas utilisée (I guess).  
```

**🌞 Affichez votre gateway**

```
PS C:\Users\vidal>ipconfig /all
[...]
- Carte réseau sans fil Wi-Fi :
    Passerelle par défaut. . . . . . . . . : 10.33.19.254  
```
  
### En graphique (GUI : Graphical User Interface)

En utilisant l'interface graphique de votre OS : 


**🌞 Trouvez comment afficher les informations sur une carte IP (change selon l'OS)**

- Je vais passer par le Panneau de Configuration:  
    -> Path: Panneau de configuration\Réseau et Internet\Centre Réseau et partage    
    -> Détail du réseau actif (ynov WIFI)    
    -> IP: ![pi-adress](./ressources-png/ip-adress.png)  
    -> MAC: ![mac-adress](./ressources-png/mac-adress.png)  
    -> Passerelle: ![ip-gateway](./ressources-png/gateway-ip.png)  


### Questions

- 🌞 à quoi sert la gateway dans le réseau d'YNOV ?

Elle permet à mon laptop de communiquer vers d'autres LAN, ou plus simplement avoir accès à internet.

## 2. Modifications des informations

### A. Modification d'adresse IP (part 1)  

🌞 Utilisez l'interface graphique de votre OS pour **changer d'adresse IP** :

- changez l'adresse IP de votre carte WiFi pour une autre:  
    /!\ SCREEN A METTRE 


🌞 **Il est possible que vous perdiez l'accès internet.**  

Si l'on perd son accès à internet c'est surement du au fait que notre IP choisie est déjà utilisée par une autre carte réseau et est donc prioritaire sur la notre.

---

## II. Exploration locale en duo

### 3. Modification d'adresse IP

- Si vos PCs ont un port RJ45 alors y'a une carte réseau Ethernet associée
```
PS C:\Users\xouxo> ping 192.168.0.1

Envoi d’une requête 'Ping'  192.168.0.1 avec 32 octets de données :
Réponse de 192.168.0.1 : octets=32 temps<1ms TTL=128
Réponse de 192.168.0.1 : octets=32 temps<1ms TTL=128
Réponse de 192.168.0.1 : octets=32 temps<1ms TTL=128
Réponse de 192.168.0.1 : octets=32 temps<1ms TTL=128

Statistiques Ping pour 192.168.0.1:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 0ms, Maximum = 0ms, Moyenne = 0ms
```
```
PS C:\Users\xouxo> arp -a
[...]
Interface : 192.168.0.2 --- 0xa
  Adresse Internet      Adresse physique      Type
  192.168.0.1           e4-a8-df-ff-9d-74     dynamique
  192.168.0.3           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
```

### 4. Utilisation d'un des deux comme gateway

- utiliser un traceroute ou tracert pour bien voir que les requêtes passent par la passerelle choisie (l'autre le PC)

```
PS C:\Users\garra> tracert  -4 8.8.8.8

Détermination de l’itinéraire vers dns.google [8.8.8.8]
avec un maximum de 30 sauts :

  1     2 ms     *        2 ms  LAPTOP-UORD0VVO [192.168.137.1]
  2     *        *        *     Délai d’attente de la demande dépassé.
  3     4 ms     4 ms     3 ms  10.33.19.254
  4     8 ms     8 ms     7 ms  137.149.196.77.rev.sfr.net [77.196.149.137]
  5    11 ms    16 ms    12 ms  108.97.30.212.rev.sfr.net [212.30.97.108]
  6    22 ms    22 ms    22 ms  222.172.136.77.rev.sfr.net [77.136.172.222]
  7    22 ms    24 ms    23 ms  221.172.136.77.rev.sfr.net [77.136.172.221]
  8    24 ms    27 ms    28 ms  186.144.6.194.rev.sfr.net [194.6.144.186]
  9    25 ms    25 ms    24 ms  186.144.6.194.rev.sfr.net [194.6.144.186]
 10    23 ms    23 ms    25 ms  72.14.194.30
 11    27 ms    26 ms    25 ms  172.253.69.49
 12    25 ms    25 ms    25 ms  108.170.238.107
 13    26 ms    25 ms    26 ms  dns.google [8.8.8.8]

Itinéraire déterminé.
```

### 5. Petit chat privé

- sur le PC serveur avec par exemple l'IP 192.168.1.1


```
PS C:\Users\xouxo\Desktop\netcat-1.11> ./nc.exe -l -p 8888
fglfjgfgdxxcc vvbvvvcvvvvvv
cvc
v
v
v
vv
v
 v
Salut
COomment sa vas
```

- sur le PC client avec par exemple l'IP 192.168.1.2

```
PS C:\Users\garra\Desktop\netcat-1.11> ./nc.exe 192.168.0.2 8888
Salut
fglfjgfgdxxcc vvbvvvcvvvvvv
cvc
v
v
COomment sa vas
```

- pour aller un peu plus loin

```
PS C:\Users\xouxo\Desktop\netcat-1.11>./nc.exe -l -p 8888 192.168.0.1
HOLLA
COMO ALLEZ VU
UI
UI
UWU
UW
U
W
UWU
PS C:\Users\xouxo\Desktop\netcat-1.11> ./nc.exe -l -p 8888 192.168.0.4
invalid connection to [192.168.0.2] from (UNKNOWN) [192.168.0.1] 50903
```

### 6. Firewall

- Autoriser les ping

```
PS C:\Users\xouxo> netsh advfirewall firewall add rule name="ICMP Allow incoming V4 echo request" protocol=icmpv4:8,any dir=in action=allow
Ok.
```

- Autoriser le traffic sur le port qu'utilise nc
```
PS C:\Users\xouxo> netsh advfirewall firewall add rule name="ALLOW TCP PORT 1050" dir=in action=allow protocol=TCP localport=1050
Ok.

PS C:\Users\xouxo> cd .\Desktop\netcat-1.11\
PS C:\Users\xouxo\Desktop\netcat-1.11> .\nc.exe -l -p 1050
dd
d
d
```

# III. Manipulations d'autres outils/protocoles côté client

## 1. DHCP 

🌞Exploration du DHCP, depuis votre PC

- afficher l'adresse IP du serveur DHCP du réseau WiFi YNOV
```
PS C:\Users\vidal> ipconfig /all
[...]
Carte réseau sans fil Wi-Fi :
Serveur DHCP . . . . . . . . . . . . . : 10.33.19.254
```
- cette adresse a une durée de vie limitée. C'est le principe du ***bail DHCP*** (ou *DHCP lease*). Trouver la date d'expiration de votre bail DHCP  

```
PS C:\Users\vidal> ipconfig /all
[...]
Carte réseau sans fil Wi-Fi :
Bail obtenu. . . . . . . . . . . . . . : mercredi 28 septembre 2022 08:51:14
   Bail expirant. . . . . . . . . . . . . : jeudi 29 septembre 2022 08:50:32
```

## 2. DNS

- 🌞 trouver l'adresse IP du serveur DNS que connaît votre ordinateur
```
PS C:\Users\vidal>ipconfig /all
[...]
Carte réseau sans fil Wi-Fi :
 Serveurs DNS. . .  . . . . . . . . . . : 8.8.8.8
                                       8.8.4.4
                                       1.1.1.1
```

- 🌞 utiliser, en ligne de commande l'outil `nslookup` (Windows, MacOS) ou `dig` (GNU/Linux, MacOS) pour faire des requêtes DNS à la main

  - faites un *lookup* 
    - pour `google.com`:
      ```
      PS C:\Users\vidal> nslookup google.com
      Serveur :   dns.google
      Address:  8.8.8.8

      Réponse ne faisant pas autorité :
      Nom :    google.com
      Addresses:  2a00:1450:4007:808::200e
                  216.58.215.46
      ```
    - pour `ynov.com`:  
    ```
      PS C:\Users\vidal> nslookup ynov.com
        Serveur :   dns.google
        Address:  8.8.8.8

      Réponse ne faisant pas autorité :
      Nom :    ynov.com
      Addresses:  2606:4700:20::681a:be9
                  2606:4700:20::681a:ae9
                  2606:4700:20::ac43:4ae2
                  104.26.10.233
                  104.26.11.233
                  172.67.74.226
    ```

    - interpréter les résultats de ces commandes
    Avec les trois adresses d'ynov, je suppose que ynov a trois serveurs qui redirigent `ynov.com`  

  - faites un *reverse lookup* 
    - pour l'adresse `78.74.21.21`
    ```
    PS C:\Users\vidal> nslookup 78.74.21.21
      Serveur :   dns.google
      Address:  8.8.8.8

      Nom :    host-78-74-21-21.homerun.telia.com
      Address:  78.74.21.21
      ```
    
    - pour l'adresse `92.146.54.88`  
    ```
    PS C:\Users\vidal> nslookup 92.146.54.88
      Serveur :   dns.google
      Address:  8.8.8.8

    *** dns.google ne parvient pas à trouver 92.146.54.88 : Non-existent domain
    ```

    - interpréter les résultats
    92.146.54.88 ne dois pas avoir de nom de domaine, voire ne pas exister tout court, c'est pour ça qu'il y a une erreur

# IV. Wireshark

- 🌞 utilisez le pour observer les trames qui circulent entre vos deux carte Ethernet. Mettez en évidence :
  - un `ping` entre vous et la passerelle
    ![ping](./ressources-png/ping.png)

  - un `netcat` entre vous et votre mate, branché en RJ45  
  ![ping netcat](./ressources-png/netcat.png)

  - une requête DNS. Identifiez dans la capture le serveur DNS à qui vous posez la question.
  ![ping dns](./ressources-png/pinggoogle.png)

