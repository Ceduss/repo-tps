# TP3 : On va router des trucs


## 0. Prérequis

Deux VM stup ok

## I. ARP

### 1. Echange ARP

🌞**Générer des requêtes ARP**

- effectuer un `ping` d'une machine à l'autre:  
```bash
[ced@john ~]$ ping 10.3.1.12 -c 3
PING 10.3.1.12 (10.3.1.12) 56(84) bytes of data.
64 bytes from 10.3.1.12: icmp_seq=1 ttl=64 time=0.347 ms
64 bytes from 10.3.1.12: icmp_seq=2 ttl=64 time=0.755 ms
64 bytes from 10.3.1.12: icmp_seq=3 ttl=64 time=0.947 ms

--- 10.3.1.12 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2061ms
rtt min/avg/max/mdev = 0.347/0.683/0.947/0.250 ms
```  

- observer les tables ARP des deux machines:  
```bash
[ced@john ~]$ ip n show
10.3.1.1 dev enp0s8 lladdr 0a:00:27:00:00:08 REACHABLE
10.3.1.12 dev enp0s8 lladdr  08:00:27:bd:1a:bb REACHABLE
```  
```bash
[ced@marcel ~]$ ip n show
10.3.1.11 dev enp0s8 lladdr 08:00:27:12:bf:85 REACHABLE
10.3.1.1 dev enp0s8 lladdr 0a:00:27:00:00:08 REACHABLE
```

- repérer l'adresse MAC de `john` dans la table ARP de `marcel` et vice-versa:  
    - MAC john into ARP marcel:   
        `08:00:27:12:bf:85`

    - MAC marcel into ARP john:   
        `08:00:27:bd:1a:bb`

- prouvez que l'info est correcte:   
```bash
[ced@john ~]$ ip link show
[...]

2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
link/ether 08:00:27:12:bf:85 brd ff:ff:ff:ff:ff:ff
 ```  
```bash
[ced@marcel ~]$ ip link show
[...]

2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
link/ether 08:00:27:bd:1a:bb brd ff:ff:ff:ff:ff:ff
```  

### 2. Analyse de trames

🌞**Analyse de trames**

Capture de trames:  
`sudo tcpdump arp or icmp -n -i enp0s8 -w tp3_arp.pcapng`  

🦈 **Capture réseau `tp3_arp.pcapng`** qui contient un ARP request et un ARP reply

- [ARPTRAMES](./ressources/tp3_arp.pcapng)  


## II. Routage


| Machine  | `10.3.1.0/24` | `10.3.2.0/24` |
|----------|---------------|---------------|
| `router` | `10.3.1.254`  | `10.3.2.254`  |
| `john`   | `10.3.1.11`   | no            |
| `marcel` | no            | `10.3.2.12`   |


### 1. Mise en place du routage

🌞**Activer le routage sur le noeud `router`**

```bash
[ced@router ~]$ sudo firewall-cmd --get-active-zone
public
  interfaces: enp0s8 enp0s9

[ced@router ~]$ sudo firewall-cmd --add-masquerade --zone=public --permanent
success
```


🌞**Ajouter les routes statiques nécessaires pour que `john` et `marcel` puissent se `ping`**

- il faut ajouter une seule route des deux côtés:  
```bash
[ced@marcel ~]$ sudo ip route add 10.3.1.0/24 via 10.3.2.254 dev enp0s8
```  
```bash
[ced@john ~]$ sudo ip route add 10.3.2.0/24 via 10.3.1.254 dev enp0s8
```

Vérif que ce soit permanent:  
```bash
[ced@john ~]$ cat /etc/sysconfig/network-scripts/route-enp0s8
default via 10.3.1.254 dev enp0s8
10.3.2.0/24 via 10.3.1.254 dev enp0s8
```  
```bash
[ced@marcel ~]$ cat /etc/sysconfig/network-scripts/route-enp0s8
10.3.1.0/24 via 10.3.2.254 dev enp0s8
default via 10.3.2.254 dev enp0s8
```

- une fois les routes en place, vérifiez avec un `ping` que les deux machines peuvent se joindre:  

```bash
[ced@john ~]$ ping 10.3.2.12 -c 3
PING 10.3.2.12 (10.3.2.12) 56(84) bytes of data.
64 bytes from 10.3.2.12: icmp_seq=1 ttl=63 time=0.881 ms
64 bytes from 10.3.2.12: icmp_seq=2 ttl=63 time=1.41 ms
64 bytes from 10.3.2.12: icmp_seq=3 ttl=63 time=1.71 ms

--- 10.3.2.12 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2005ms
rtt min/avg/max/mdev = 0.881/1.332/1.706/0.341 ms
```   
```bash
[ced@marcel ~]$ ping 10.3.1.11 -c 3
PING 10.3.1.11 (10.3.1.11) 56(84) bytes of data.
64 bytes from 10.3.1.11: icmp_seq=1 ttl=63 time=1.17 ms
64 bytes from 10.3.1.11: icmp_seq=2 ttl=63 time=2.27 ms
64 bytes from 10.3.1.11: icmp_seq=3 ttl=63 time=1.88 ms

--- 10.3.1.11 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2005ms
rtt min/avg/max/mdev = 1.173/1.773/2.269/0.453 ms
```


### 2. Analyse de trames

🌞**Analyse des échanges ARP**

- videz les tables ARP des trois noeuds:   
`sudo ip n flush all`

- effectuez un `ping` de `john` vers `marcel`:   
```bash
[ced@john ~]$ ping 10.3.2.12 -c 1
PING 10.3.2.12 (10.3.2.12) 56(84) bytes of data.
64 bytes from 10.3.2.12: icmp_seq=1 ttl=63 time=1.92 ms

--- 10.3.2.12 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 1.924/1.924/1.924/0.000 ms
```

- regardez les tables ARP des trois noeuds:   
```bash
[ced@john ~]$ ip n show
10.3.1.254 dev enp0s8 lladdr 08:00:27:0c:ad:85 DELAY
10.3.1.1 dev enp0s8 lladdr 0a:00:27:00:00:08 REACHABLE
```  
```bash
[ced@marcel ~]$ ip n show
10.3.2.254 dev enp0s8 lladdr 08:00:27:1c:bc:81 STALE
10.3.2.1 dev enp0s8 lladdr 0a:00:27:00:00:09 REACHABLE
```  
```bash
[ced@router ~]$ ip n show
10.3.2.12 dev enp0s9 lladdr 08:00:27:bd:1a:bb STALE
10.3.1.11 dev enp0s8 lladdr 08:00:27:12:bf:85 STALE
10.3.1.1 dev enp0s8 lladdr 0a:00:27:00:00:08 DELAY
```   

- essayez de déduire un peu les échanges ARP qui ont eu lieu: 

Pour le ping de john vers marcel, un échange ARP entre john et la passerelle a eu lieu, puis en passant par la passerelle du deuxième réseau un échange ARP en la passerelle et marcel. La request fait le chemin inverse, de marcel vers le routeur en passant par la passerelle du premier réseau puis du routeur vers john en passant par la passerelle du second réseau 

- répétez l'opération précédente (vider les tables, puis `ping`), en lançant `tcpdump` sur `marcel`:


| ordre | type trame  | IP source  | MAC source              | IP destination  | MAC destination            |
|-------|-------------|------------|-------------------------|-----------------|----------------------------|
| 1     | Requête ARP |`10.3.2.254`| `router` `08:00:27:1c:bc:81` |`10.3.2.12` | Broadcast `FF:FF:FF:FF:FF` |
| 2     | Réponse ARP |`10.3.2.12` | `marcel` `08:00:27:bd:1a:bb` |`10.3.2.254`|`router` `08:00:27:1c:bc:81`|
| 3     | Ping        |`10.3.2.254`| `router` `08:00:27:1c:bc:81` |`10.3.2.12` |`marcel` `08:00:27:bd:1a:bb`|
| 4     | Pong        |`10.3.2.12` | `marcel` `08:00:27:bd:1a:bb` |`10.3.2.254`|`router` `08:00:27:1c:bc:81`|


🦈 **Capture réseau `tp2_routage_marcel.pcapng`**  
- [ROUTAGEMARECELLE](./ressources/tp3_routage_marcel.pcapng)


### 3. Accès internet

🌞**Donnez un accès internet à vos machines**

- ajoutez une carte NAT en 3ème inteface sur le `router` pour qu'il ait un accès internet:  
```bash
[ced@router ~]$ ping 8.8.8.8 -c 1
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=247 time=19.9 ms

--- 8.8.8.8 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 19.896/19.896/19.896/0.000 ms
```

- ajoutez une route par défaut à `john` et `marcel`:  
```bash
[ced@john ~]$ sudo ip route add default via 10.3.1.254 dev enp0s8
[sudo] password for ced:
[ced@john ~]$ ping 8.8.8.8 -c 1
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=246 time=36.5 ms

--- 8.8.8.8 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 36.495/36.495/36.495/0.000 ms
```   
```bash 
[ced@marcel ~]$ sudo ip route add default via 10.3.2.254 dev enp0s8
[sudo] password for ced:
[ced@marcel ~]$ ping 8.8.8.8 -c 1
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=246 time=20.3 ms

--- 8.8.8.8 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 20.286/20.286/20.286/0.000 ms
```

- donnez leur aussi l'adresse d'un serveur DNS qu'ils peuvent utiliser:  
Pour le DNS se référer à la ligne `SERVER` ci dessous.  
```bash
[ced@marcel ~]$ dig google.com
[...]

;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             30      IN      A       216.58.214.78

;; Query time: 29 msec
;; SERVER: 8.8.8.8#53(8.8.8.8)
```   
```bash
[ced@john ~]$ ping google.com -c 1
PING google.com (216.58.214.78) 56(84) bytes of data.
64 bytes from par10s39-in-f14.1e100.net (216.58.214.78): icmp_seq=1 ttl=245 time=27.1 ms

--- google.com ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 27.122/27.122/27.122/0.000 ms
```


🌞**Analyse de trames**

- analysez un ping aller et le retour qui correspond et mettez dans un tableau :

| ordre| type trame|IP source         | MAC source               |IP destination| MAC destination |
|------|-----------|------------------|--------------------------|--------------|-----------------|
| 1    | ping      |`john` `10.3.1.12`|`john` `08:00:27:12:bf:85`| `8.8.8.8`    |`router` `08:00:27:0c:ad:85`|
| 2    | pong      | `8.8.8.8`        |`router` `08:00:27:0c:ad:85`|`john` `10.3.1.12`|`john` `08:00:27:12:bf:85`|

🦈 **Capture réseau `tp2_routage_internet.pcapng`**:  
-[ROUTAGEINTERNET](./ressources/tp3_routage_internet.pcapng)

## III. DHCP

On reprend la config précédente, et on ajoutera à la fin de cette partie une 4ème machine pour effectuer des tests.

| Machine  | `10.3.1.0/24`              | `10.3.2.0/24` |
|----------|----------------------------|---------------|
| `router` | `10.3.1.254`               | `10.3.2.254`  |
| `john`   | `10.3.1.11`                | no            |
| `bob`    | oui mais pas d'IP statique | no            |
| `marcel` | no                         | `10.3.2.12`   |

### 1. Mise en place du serveur DHCP

🌞**Sur la machine `john`, vous installerez et configurerez un serveur DHCP** (go Google "rocky linux dhcp server").

- installation du serveur sur `john`
- créer une machine `bob`
- faites lui récupérer une IP en DHCP à l'aide de votre serveur

> Il est possible d'utilise la commande `dhclient` pour forcer à la main, depuis la ligne de commande, la demande d'une IP en DHCP, ou renouveler complètement l'échange DHCP (voir `dhclient -h` puis call me et/ou Google si besoin d'aide).

🌞**Améliorer la configuration du DHCP**

- ajoutez de la configuration à votre DHCP pour qu'il donne aux clients, en plus de leur IP :
  - une route par défaut
  - un serveur DNS à utiliser
- récupérez de nouveau une IP en DHCP sur `bob` pour tester :
  - `marcel` doit avoir une IP
    - vérifier avec une commande qu'il a récupéré son IP
    - vérifier qu'il peut `ping` sa passerelle
  - il doit avoir une route par défaut
    - vérifier la présence de la route avec une commande
    - vérifier que la route fonctionne avec un `ping` vers une IP
  - il doit connaître l'adresse d'un serveur DNS pour avoir de la résolution de noms
    - vérifier avec la commande `dig` que ça fonctionne
    - vérifier un `ping` vers un nom de domaine

### 2. Analyse de trames

🌞**Analyse de trames**

- lancer une capture à l'aide de `tcpdump` afin de capturer un échange DHCP
- demander une nouvelle IP afin de générer un échange DHCP
- exportez le fichier `.pcapng`

🦈 **Capture réseau `tp2_dhcp.pcapng`**