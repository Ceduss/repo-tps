J'ai choisi le projet sécu MITM

Ce n'était pas mon choix de base, j'aime beaucoup la cybersécu (bien que je ne sois qu'un novice complet).  
Je ne suis pas un dev et j'ai énormément de difficultés pour cette discipline.  
Je voulais donc choisir le VPN car j'avais déjà setup ce genre de projet et ce choix de la facilité m'attirait beacoup.  
J'ai voulu me challenger et faire le sujet de dev pour me permettre de sortir de ma zone de confort et d'acquérir un savoir qui n'est pas ce à quoi je touche habituellement.  

Jje n'ai pas réussi à finir ce projet, je vais malgrés tout expliquer ce que j'ai fais pour essayer de montrer que c'est mon imcompétence et non une féneantise qui à pour conséquence mon échec.  

Il y à deux fichiers, MITM.py qui est le code que j'ai tenté à 100% de dev par mes propres moyens (incomplet) et MITM2.py qui est juste l'endroit ou je copy/paste des codes trouvés pour essayer de les comprendre et les décortiquer.  

J'ai donc commencé par coder un scanner qui me permet de trouver quelles IP sont connectés aux réseaux ciblés.  
Cette parti est nécessaire pour permettre au script de récupérer les potentielles cibles.  
J'ai utilisé du python avec la lib `Scapy`.  
C'est au moment de setup le MITM que je n'ai pas réussi car en faisant mes recherches, tous les exemples que j'ai pu trouver était soit trop spécifique à l'utilisation du code fourni, soit ne répondais pas à mon besoin spécifique.  
Comme mes recherches ne sont pas encore très efficaces et accurate, plus le fait que ma capacité a dev soit très limitée, je me suis vite retrouvé bloqué.  

Je suis déçu de ne pas avoir réussi à finir ce projet, d'une part par égo, car j'aurais voulu prouver que mes lacunes pouvaient être surpassées.  
C'est évident que ce genre de choses prennent du temps et que je pouvais pas réussir à apprendre en 1 semaine.  
Et d'une autre part car je sais que je risque de me prendre une caisse et que ça risque de me compliquer l'année.  
Cependant je suis quand même content de ne pas avoir fais le choix de la facilité et d'avoir essayé le sujet cyber/dev.  
J'ai découvert une lib puissante et j'ai malgrès tout évolué, le savoir que j'en ai tiré me vaut largement le négatifs subsécant.   

Merci d'avoir pris la peine d'avoir fait ce sujet, même si le résultat est médiocre il m'a poussé dans mes retranchements et j'en ai retiré beaucoup de savoir et de travail ! 