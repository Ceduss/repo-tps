# Module 1 : Reverse Proxy

# I. Intro

# II. Setup


➜ **On utilisera NGINX comme reverse proxy**

- installer le paquet `nginx`:  
```bash
[ced@proxy ~]$ sudo dnf install nginx -y

Installed:
  nginx-1:1.20.1-10.el9.x86_64                   nginx-filesystem-1:1.20.1-10.el9.noarch

Complete!
```

- démarrer le service `nginx`:  
```bash
[ced@proxy ~]$ sudo systemctl start nginx
[ced@proxy ~]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
     Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; vendor preset: disabled)
     Active: active (running) since Mon 2022-11-21 11:27:43 CET; 7s ago
```

- utiliser la commande `ss` pour repérer le port sur lequel NGINX écoute:  
```bash
[ced@proxy ~]$ sudo ss -alnpt | grep nginx
LISTEN 0      511          0.0.0.0:80        0.0.0.0:*    users:(("nginx",pid=1531,fd=6),("nginx",pid=1530,fd=6))
LISTEN 0      511             [::]:80           [::]:*    users:(("nginx",pid=1531,fd=7),("nginx",pid=1530,fd=7))
```

- ouvrir un port dans le firewall pour autoriser le trafic vers NGINX:  
```bash
[ced@proxy ~]$ sudo firewall-cmd --list-all
public (active)
[...]

  ports: 80/tcp
```

- utiliser une commande `ps -ef` pour déterminer sous quel utilisateur tourne NGINX:  
```bash 
[ced@proxy ~]$ ps -ef | grep nginx
root        1530       1  0 11:27 ?        00:00:00 nginx: master process /usr/sbin/nginx
nginx       1531    1530  0 11:27 ?        00:00:00 nginx: worker process
```
- vérifier que le page d'accueil NGINX est disponible en faisant une requête HTTP sur le port 80 de la machine:  
```bash
[ced@proxy ~]$ curl localhost
<!doctype html>
<html>
[...]
</html>
```


➜ **Configurer NGINX**


- deux choses à faire :  
- créer un fichier de configuration NGINX  
```bash
[ced@proxy ~]$ sudo cat /etc/nginx/nginx.conf | grep *.conf
    include /etc/nginx/conf.d/*.conf;
```

```bash
[ced@proxy ~]$ sudo cat /etc/nginx/conf.d/proxy.conf
server {
    server_name web.tp2.linux;

        listen 80;

    location / {
        proxy_set_header  Host $host;
        proxy_set_header  X-Real-IP $remote_addr;
        proxy_set_header  X-Forwarded-Proto https;
        proxy_set_header  X-Forwarded-Host $remote_addr;
        proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;

        proxy_pass http://10.102.1.11:80;
    }


    location /.well-known/carddav {
      return 301 $scheme://$host/remote.php/dav;
    }

    location /.well-known/caldav {
      return 301 $scheme://$host/remote.php/dav;
    }
}
```


- NextCloud est un peu exigeant, et il demande à être informé si on le met derrière un reverse proxy
- y'a donc un fichier de conf NextCloud à modifier
- c'est un fichier appelé `config.php`
```bash
[ced@web ~]$ sudo find / -name config.php
/var/www/tp2_nextcloud/config/config.php
```
```bash 
[ced@web ~]$ sudo cat /var/www/tp2_nextcloud/config/config.php
  'trusted_domains' =>
  array (
    0 => '10.102.1.13',
  ),
  'proxy' => '10.102.1.13:80',
```

```bash 
[ced@web ~]$ sudo systemctl restart httpd
[ced@web ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
     Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
    Drop-In: /usr/lib/systemd/system/httpd.service.d
             └─php81-php-fpm.conf
     Active: active (running) since Mon 2022-11-21 11:56:48 CET; 6s ago
```  



✨ **Bonus** : rendre le serveur `web.tp2.linux` injoignable sauf depuis l'IP du reverse proxy. En effet, les clients ne doivent pas joindre en direct le serveur web : notre reverse proxy est là pour servir de serveur frontal. Une fois que c'est en place :

- faire un `ping` manuel vers l'IP de `proxy.tp3.linux` fonctionne:  
```bash
[ced@db ~]$ ping 10.102.1.13 -c 3
PING 10.102.1.13 (10.102.1.13) 56(84) bytes of data.
64 bytes from 10.102.1.13: icmp_seq=1 ttl=64 time=0.457 ms
64 bytes from 10.102.1.13: icmp_seq=2 ttl=64 time=0.542 ms
64 bytes from 10.102.1.13: icmp_seq=3 ttl=64 time=0.645 ms

--- 10.102.1.13 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2080ms
rtt min/avg/max/mdev = 0.457/0.548/0.645/0.076 ms
```

- faire un `ping` manuel vers l'IP de `web.tp2.linux` ne fonctionne pas:  
```bash
[ced@db ~]$ ping 10.102.1.11 -c 3
PING 10.102.1.11 (10.102.1.11) 56(84) bytes of data.
From 10.102.1.11 icmp_seq=1 Destination Port Unreachable
From 10.102.1.11 icmp_seq=2 Destination Port Unreachable
From 10.102.1.11 icmp_seq=3 Destination Port Unreachable

--- 10.102.1.11 ping statistics ---
3 packets transmitted, 0 received, +3 errors, 100% packet loss, time 2041ms
```


# III. HTTPS

Le but de cette section est de permettre une connexion chiffrée lorsqu'un client se connecte. Avoir le ptit HTTPS :)

Le principe :

- on génère une paire de clés sur le serveur `proxy.tp3.linux`
  - une des deux clés sera la clé privée : elle restera sur le serveur et ne bougera jamais
  - l'autre est la clé publique : elle sera stockée dans un fichier appelé *certificat*
    - le *certificat* est donné à chaque client qui se connecte au site

```bash
[ced@proxy ssl]$ sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/nginx-selfsigned.key -out /etc/ssl/certs/nginx-selfsigned.crt
..+..+...+..........+...+.........+..+..................+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*.+.........+.+.....+....+.........+.....+......+....+..+......+...+......+...............+....+.....+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*.......+............+..+.+..................+...........+...+.......+.....+......+...+.+...+...+..+.+.........+...........+......+.+.....+....+...+..........................+....+..+......+.........+....+..............+...+...+....+.....+.+...........+......+.......+........+...+...................+..+.+.....+....+...+..+......+....+......+..+......+.......+..+.+......+.....+..........+........+............+....+.....+....+..+...............+...+.+.........+...+.....+.......+...........+......+....+............+........+.+.....+......+...+.........+.+....................+.......+.....+..........+..............+......+....+...+...............+..+......+...+..........+..+....+...+.....+.+..+.......+......+..+...+.........+......+......+...+.......+...+..+..........+......+...+..+.............+......+.........+..+...+....+............+.....................+...+..+....+......+.....+...+....+...........+.........+..........+.....+..........+.....+.+.....+.+.....+...+.+..............+...+....+...+..+.............+...+.....+..........+..................+......+......+...+.....+....+......+.....+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.+..+..........+...+..+......+...+.+...+...+..+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*.+...+.....+...+....+......+.....+.+.....+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*........+..+..........+.....+.+..+............+.......+.....+.+...............+.....+...+....+.....+......+.+.....+......+..........+.................+...........................+...+.......+.....+.+.....+............+...+...+......+....+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [XX]:FR
State or Province Name (full name) []:France
Locality Name (eg, city) [Default City]:Bordeaux
Organization Name (eg, company) [Default Company Ltd]:Ynov
Organizational Unit Name (eg, section) []:Ynov
Common Name (eg, your name or your server's hostname) []:Ynov
Email Address []:ynov.ynov@ynov.com
```  

```bash
[ced@proxy ssl]$ sudo openssl dhparam -out /etc/nginx/dhparam.pem 4096
Generating DH parameters, 4096 bit long safe prime
.......................................................................................................................................................+.....+.........................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................+........................................................................................................................................................................................+..............................................+.........++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*
```


- on ajuste la conf NGINX
  - on lui indique le chemin vers le certificat et la clé privée afin qu'il puisse les utiliser pour chiffrer le trafic

  ```bash
  [ced@proxy ~]$ sudo vim /etc/nginx/conf.d/conf.conf
  server {
            server_name web.tp2.linux;

            listen 443;
            listen [::]:443 ssl;
            include snippets/self-signed.conf;
            include snippets/ssl-params.conf;
            location / {
                    proxy_set_header  Host $host;
                    proxy_set_header  X-Real-IP $remote_addr;
                    proxy_set_header  X-Forwarded-Proto https;
                    proxy_set_header  X-Forwarded-Host $remote_addr;
                    proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;

                    proxy_pass http://10.102.1.11:80;
                                                                                }

                location /.well-known/carddav {
              return 301 $scheme://$host/remote.php/dav;
                                          }

            location /.well-known/caldav {
                 return 301 $scheme://$host/remote.php/dav;
                                              }
}
```  

```bash
[ced@web ~]$ sudo cat /var/www/tp2_nextcloud/config/config.php
[sudo] password for ced:
<?php
$CONFIG = array (
  'instanceid' => 'ocb1kut1l8fp',
  'passwordsalt' => 'n+0bMXd4haGiiVkKgPJPEJ6SlnVYJn',
  'secret' => '+USAtRP+YFhLafRJDVe+05+eNpwHKb67bC9td5cQCnCruEyM',
  'trusted_domains' =>
  array (
    0 => '10.102.1.11',
  ),
  'proxy' => '10.102.1.13',
  'datadirectory' => '/var/www/tp2_nextcloud/data',
  'dbtype' => 'mysql',
  'version' => '25.0.0.15',
  'overwrite.cli.url' => 'http://10.102.1.11',
  'overwriteprotocol' => 'https',
  'dbname' => 'nextcloud',
  'dbhost' => 'localhost',
  'dbport' => '',
  'dbtableprefix' => 'oc_',
);
```

## Module 2 : Replication base de données

Installation mariadb:  
```bash
[ced@dbslave ~]$ sudo dnf install mariadb -y
Last metadata expiration check: 0:22:21 ago on Mon 09 Jan 2023 20:29:27 CET.
Package mariadb-3:10.5.16-2.el9_0.x86_64 is already installed.
Dependencies resolved.
Nothing to do.
Complete!
```

On en fais un service:  
```bash
[ced@dbslave ~]$ sudo systemctl start mariadb
[ced@dbslave ~]$ sudo systemctl enable mariadb
[ced@dbslave ~]$ sudo systemctl status mariadb
● mariadb.service - MariaDB 10.5 database server
     Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; vendor preset: disabled)
     Active: active (running) since Mon 2023-01-09 20:49:30 CET; 4min 41s ago
       Docs: man:mariadbd(8)
             https://mariadb.com/kb/en/library/systemd/

[ced@dbslave ~]$ sudo mysql_secure_installation
```

Si il n'y a pas de retour sur le `enable` c'est aprce que j'ai cloné le db1 et mariadb était déjà installée.  


On conf la db master:  
```bash
[ced@db ~]$ sudo cat /etc/my.cnf.d/mariadb-server.cnf

[server]
[mysqld]
datadir=/var/lib/mysql
socket=/var/lib/mysql/mysql.sock
log-error=/var/log/mariadb/mariadb.log
pid-file=/run/mariadb/mariadb.pid

[galera]
[embedded]
[mariadb]
log-bin
server_id=1
log-basename=master1
binlog-format=mixed
```

On restart le service plus check:  
```bash
[ced@db ~]$ sudo systemctl  restart mariadb
[ced@db ~]$ sudo systemctl  status mariadb
● mariadb.service - MariaDB 10.5 database server
     Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; vendor preset: disabled)
     Active: active (running) since Mon 2023-01-09 21:07:07 CET; 9s ago
       Docs: man:mariadbd(8)
             https://mariadb.com/kb/en/library/systemd/
```

On verifie le port d'écoute:  
```bash
[ced@db ~]$ ss -antpl
State       Recv-Q      Send-Q           Local Address:Port           Peer Address:Port      Process
LISTEN      0           80                           *:3306                      *:*
```  

ON créer le user pour la replication dans la BD
```bash
[ced@db ~]$ sudo mysql -u root
```  
```sql
MariaDB [(none)]> CREATE USER 'replication'@'10.102.1.22' identified by 'slave';
Query OK, 0 rows affected (0.012 sec)

MariaDB [(none)]> GRANT REPLICATION SLAVE ON *.* TO 'replication'@'10.102.1.22';
Query OK, 0 rows affected (0.001 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.001 sec)
```

On conf aussi sur la slave:  
```bash
[ced@dbslave ~]$ sudo cat /etc/my.cnf.d/mariadb-server.cnf
[server]

[mysqld]
datadir=/var/lib/mysql
socket=/var/lib/mysql/mysql.sock
log-error=/var/log/mariadb/mariadb.log
pid-file=/run/mariadb/mariadb.pid
[galera]
[embedded]
[mariadb]
log-bin
server_id=2
log-basename=slave1
binlog-format=mixed
```

```sql
[ced@dbslave ~]$ sudo mysql -u root -p

MariaDB [(none)]> STOP SLAVE;
Query OK, 0 rows affected, 1 warning (0.000 sec)

MariaDB [(none)]> CHANGE MASTER TO MASTER_HOST = '10.102.1.12', MASTER_USER = 'replication', MASTER_PASSWORD = 'master', MASTER_LOG_FILE = 'master1-bin.000002', MASTER_LOG_POS = 809;
Query OK, 0 rows affected (0.014 sec)

MariaDB [(none)]> START SLAVE;
Query OK, 0 rows affected (0.001 sec)

MariaDB [(none)]> exit
Bye
```

```bash
[ced@db ~]$ sudo mysqldump -u root nextcloud > test.sql

[ced@db ~]$ cat test.sql
-- MariaDB dump 10.19  Distrib 10.5.16-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: nextcloud
-- ------------------------------------------------------
-- Server version       10.5.16-MariaDB-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-01-09 21:30:08
```

On test si ça marche:  
```sql
[ced@db ~]$ sudo mysql -u root


MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS imherefrommaster CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.000 sec)

MariaDB [(none)]> show databases ;
+--------------------+
| Database           |
+--------------------+
| imherefrommaster   |
| information_schema |
| mysql              |
| nextcloud          |
| performance_schema |
+--------------------+
5 rows in set (0.012 sec)
```

On regarde sur la slave:  
```sql
[ced@dbslave ~]$ sudo mysql -u root -p
MariaDB [(none)]> show databases;
+--------------------+
| Database           |
+--------------------+
| imherefrommaster   |
| information_schema |
| mysql              |
| nextcloud          |
| performance_schema |
+--------------------+
5 rows in set (0.001 sec)
```  

Et maintenant l'inverse:  
```sql
[ced@dbslave ~]$ sudo mysql -u root -p
MariaDB [nextcloud]> CREATE DATABASE IF NOT EXISTS ImHereFromSlave CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.000 sec)

MariaDB [nextcloud]> show databases
    -> ;
+--------------------+
| Database           |
+--------------------+
| ImHereFromSlave    |
| imherefrommaster   |
| information_schema |
| mysql              |
| nextcloud          |
| performance_schema |
+--------------------+
5 rows in set (0.001 sec)
```

Et on vérifie:  
```sql
[ced@db ~]$ sudo mysql -u root -p
MariaDB [(none)]> show databases;
+--------------------+
| Database           |
+--------------------+
| ImHereFromSlave    |
| imherefrommaster   |
| information_schema |
| mysql              |
| nextcloud          |
| performance_schema |
+--------------------+
5 rows in set (0.001 sec)
