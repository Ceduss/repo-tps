# TP2 : Gestion de service

# I. Un premier serveur web

## 1. Installation

🖥️ **VM web.tp2.linux**

| Machine         | IP            | Service     |
|-----------------|---------------|-------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web |

🌞 **Installer le serveur Apache**

- paquet `httpd`:  
```bash
[ced@web ~]$ sudo dnf install httpd
Complete!
```
- la conf se trouve dans `/etc/httpd/`
  - le fichier de conf principal est `/etc/httpd/conf/httpd.conf`:  
```bash
[ced@web ~]$ sudo cat /etc/httpd/conf/httpd.conf

ServerRoot "/etc/httpd"

Listen 80

Include conf.modules.d/*.conf

User apache
Group apache
```

🌞 **Démarrer le service Apache**

- le service s'appelle `httpd` (raccourci pour `httpd.service` en réalité)
  - démarrez le:  
  ```bash
  [ced@web ~]$ sudo systemctl start httpd
  [ced@web ~]$ sudo systemctl status httpd
     Active: active (running) since Tue 2022-11-15 10:34:47 CET; 10s ago
  ```  
  - faites en sorte qu'Apache démarre automatique au démarrage de la machine:  
  ```bash
  [ced@web ~]$ sudo systemctl enable httpd
  Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.
  ```  
  - ouvrez le port firewall nécessaire:  
  ```bash
  [ced@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
  ```  
- utiliser une commande `ss` pour savoir sur quel port tourne actuellement Apache:  
```bash
[ced@web ~]$ sudo ss -tulpn | grep httpd
tcp   LISTEN 0      511                *:80           *:*    users:(("httpd",pid=33732,fd=4),("httpd",pid=33731,fd=4),("httpd",pid=33730,fd=4),("httpd",pid=33728,fd=4))
```


🌞 **TEST**

- vérifier que le service est démarré:  
```bash
[ced@web ~]$ sudo systemctl status httpd
     Active: active (running) since Tue 2022-11-15 10:46:28 CET; 50s ago
```  
- vérifier qu'il est configuré pour démarrer automatiquement:  
```bash
[ced@web ~]$ sudo systemctl status httpd | grep enabled
     Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
```  
- vérifier avec une commande `curl localhost` que vous joignez votre serveur web localement:  
```bash
[ced@web ~]$ curl localhost
<!doctype html>
<html>
[...]
</html>
```  
- vérifier avec votre navigateur (sur votre PC) que vous accéder à votre serveur web:  
Grosse erreur sur windows mais cela fonctionne malgrés tout (vérifié)  

## 2. Avancer vers la maîtrise du service

🌞 **Le service Apache...**

- affichez le contenu du fichier `httpd.service` qui contient la définition du service Apache:  
```bash
[ced@web ~]$ sudo cat /etc/systemd/system/multi-user.target.wants/httpd.service
[Unit]
Description=The Apache HTTP Server
Wants=httpd-init.service
After=network.target remote-fs.target nss-lookup.target httpd-init.service
Documentation=man:httpd.service(8)

[Service]
Type=notify
Environment=LANG=C

ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
KillSignal=SIGWINCH
KillMode=mixed
PrivateTmp=true
OOMPolicy=continue

[Install]
WantedBy=multi-user.target
```

🌞 **Déterminer sous quel utilisateur tourne le processus Apache**

- mettez en évidence la ligne dans le fichier de conf principal d'Apache (`httpd.conf`) qui définit quel user est utilisé:  
```bash
[ced@web ~]$ sudo cat /etc/httpd/conf/httpd.conf | grep User
User apache
```  
- utilisez la commande `ps -ef` pour visualiser les processus en cours d'exécution et confirmer que apache tourne bien sous l'utilisateur mentionné dans le fichier de conf:  
```bash
[ced@web ~]$ ps -ef | grep apache
apache     33136   33135  0 10:09 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache     33137   33135  0 10:09 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache     33138   33135  0 10:09 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache     33139   33135  0 10:09 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
```
- la page d'accueil d'Apache se trouve dans `/usr/share/testpage/`
  - vérifiez avec un `ls -al` que tout son contenu est **accessible en lecture** à l'utilisateur mentionné dans le fichier de conf  
```bash
[ced@web ~]$ cd /usr/share/testpage
[ced@web testpage]$ ls -al
-rw-r--r--.  1 root root 7620 Jul  6 04:37 index.html
```

🌞 **Changer l'utilisateur utilisé par Apache**

- créez un nouvel utilisateur:  
```bash
[ced@web ~]$ sudo cat /etc/passwd
apache:x:51:51:Apache:/usr/share/httpd:/sbin/nologin
```  
- modifiez la configuration d'Apache pour qu'il utilise ce nouvel utilisateur:  
```bash
[ced@web ~]$ sudo useradd NewUser -m -s /sbin/nologin
[ced@web ~]$ sudo cat /etc/passwd
NewUser:x:1001:1002::/home/NewUser:/sbin/nologin
```  
```bash
[ced@web ~]$ sudo cat /etc/httpd/conf/httpd.conf
User NewUser
```
- redémarrez Apache:  
```bash
[ced@web ~]$ sudo systemctl restart httpd
[ced@web ~]$ ps -ef
NewUser   33491   33490  0 10:43 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
NewUser   33492   33490  0 10:43 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
NewUser   33493   33490  0 10:43 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
NewUser   33494   33490  0 10:43 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
```  

🌞 **Faites en sorte que Apache tourne sur un autre port**

- modifiez la configuration d'Apache pour lui demander d'écouter sur un autre port de votre choix:  
```bash
[ced@web ~]$ sudo cat /etc/httpd/conf/httpd.conf
Listen 8080
```  
- ouvrez ce nouveau port dans le firewall, et fermez l'ancien:  
```bash
[ced@web ~]$ sudo firewall-cmd --add-port=8080/tcp --permanent
success
[ced@web ~]$ sudo firewall-cmd --reload
success
```  
- redémarrez Apache:  
```bash
[ced@web ~]$ sudo systemctl restart httpd
```  
- prouvez avec une commande `ss` que Apache tourne bien sur le nouveau port choisi:  
```bash
[ced@web ~]$ sudo ss -tulpn | grep httpd
tcp   LISTEN 0      511                *:8080            *:*    users:(("httpd",pid=33732,fd=4),("httpd",pid=33731,fd=4),("httpd",pid=33730,fd=4),("httpd",pid=33728,fd=4))
```  
- vérifiez avec `curl` en local que vous pouvez joindre Apache sur le nouveau port:  
```bash
[ced@web ~]$ curl localhost:8080
<!doctype html>
<html>
[...]
</html>
```  
- vérifiez avec votre navigateur que vous pouvez joindre le serveur sur le nouveau port:  
izoké

📁 **Fichier `/etc/httpd/conf/httpd.conf`**

# II. Une stack web plus avancée

## 2. Setup

🖥️ **VM db.tp2.linux**

### A. Base de données

🌞 **Install de MariaDB sur `db.tp2.linux`**

 ```bash
[ced@db ~]$ sudo dnf install mariadb-server -y
Complete!
```  
```bash
[ced@db ~]$ sudo systemctl enable mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.
```  
```bash
[ced@db ~]$ sudo systemctl start mariadb
[ced@db ~]$ sudo systemctl status mariadb
    Active: active (running) since Tue 2022-11-15 11:57:59 CET; 31s ago
```  
```bash
[ced@db ~]$ sudo mysql_secure_installation
Switch to unix_socket authentication [Y/n] y
Change the root password? [Y/n] y
Remove anonymous users? [Y/n] y
Disallow root login remotely? [Y/n] y
Remove test database and access to it? [Y/n] y
Reload privilege tables now? [Y/n] y
```  
```bash
[ced@db ~]$ sudo ss -tulpn | grep mariadbd
tcp   LISTEN 0      80                 *:3306            *:*    users:(("mariadbd",pid=3115,fd=18))
```  
```bash
[ced@db ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent
success
[ced@db ~]$ sudo firewall-cmd --reload
success
```  

🌞 **Préparation de la base pour NextCloud**

- une fois en place, il va falloir préparer une base de données pour NextCloud :
  - connectez-vous à la base de données à l'aide de la commande `sudo mysql -u root -p`:  
```bash
[ced@db ~]$ sudo mysql -u root -p

MariaDB [(none)]> CREATE USER 'nextcloud'@'10.102.1.11' IDENTIFIED BY 'pewpewpew';
Query OK, 0 rows affected (0.001 sec)

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.000 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.102.1.11';
Query OK, 0 rows affected (0.002 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.001 sec)
```  

🌞 **Exploration de la base de données**  

```bash
[ced@web ~]$ sudo dnf install mysql
Complete!
```  

```bash
[ced@web ~]$ sudo mysql -u nextcloud -h 10.102.1.12 -p

mysql> show DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| nextcloud          |
+--------------------+
2 rows in set (0.00 sec)
```  

```sql
mysql> USE information_schema
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
```  

```sql
mysql> SHOW TABLES;
+---------------------------------------+
| Tables_in_information_schema          |
+---------------------------------------+
```


🌞 **Trouver une commande SQL qui permet de lister tous les utilisateurs de la base de données**

```sql
MariaDB [(none)]> select user, host from mysql.user;
+-------------+-------------+
| User        | Host        |
+-------------+-------------+
| newtcloud   | 10.102.1.11 |
| nextcloud   | 10.102.1.11 |
| mariadb.sys | localhost   |
| mysql       | localhost   |
| root        | localhost   |
+-------------+-------------+
5 rows in set (0.001 sec)
```

### B. Serveur Web et NextCloud

⚠️⚠️⚠️ **N'OUBLIEZ PAS de réinitialiser votre conf Apache avant de continuer. En particulier, remettez le port et le user par défaut.**

🌞 **Install de PHP**

```bash
[ced@web ~]$ sudo dnf config-manager --set-enabled crb
[ced@web ~]$ sudo dnf install dnf-utils http://rpms.remirepo.net/enterprise/remi-release-9.rpm -y
Complete!
```  

```bash
[ced@web ~]$ dnf module list php
php          remi-8.2         common [d], devel, minimal         PHP scripting language

Hint: [d]efault, [e]nabled, [x]disabled, [i]nstalled

[ced@web ~]$ sudo dnf module enable php:remi-8.1 -y
Complete!

[ced@web ~]$ sudo dnf install -y php81-php
Complete!

[ced@web ~]$  sudo dnf install -y libxml2 openssl php81-php php81-php-ctype php81-php-curl php81-php-gd php81-php-iconv php81-php-json php81-php-libxml php81-php-mbstring php81-php-openssl php81-php-posix php81-php-session php81-php-xml php81-php-zip php81-php-zlib php81-php-pdo php81-php-mysqlnd php81-php-intl php81-php-bcmath php81-php-gmp
Complete!
```

🌞 **Récupérer NextCloud**

- créez le dossier `/var/www/tp2_nextcloud/`:  
```bash
[ced@web ~]$ sudo mkdir /var/www/tp2_nextcloud
```

- récupérer le fichier suivant avec une commande `curl` ou `wget` : https://download.nextcloud.com/server/prereleases/nextcloud-25.0.0rc3.zip:  
- extrayez tout son contenu dans le dossier `/var/www/tp2_nextcloud/` en utilisant la commande `unzip`:  
```bash
[ced@web www]$ sudo wget https://download.nextcloud.com/server/prereleases/nextcloud-25.0.0rc3.zip
[ced@web www]$ sudo unzip nextcloud-25.0.0rc3.zip
inflating: nextcloud/config/.htaccess
```

- contrôlez que le fichier `/var/www/tp2_nextcloud/index.html` existe pour vérifier que tout est en place:  
```bash
[ced@web tp2_nextcloud]$ ls |grep index
index.html
index.php
```
- assurez-vous que le dossier `/var/www/tp2_nextcloud/` et tout son contenu appartient à l'utilisateur qui exécute le service Apache:  
```bash
[ced@web tp2_nextcloud]$ ls -al
total 136
drwxr-xr-x. 16 apache apache  4096 Nov 18 12:43 .
drwxr-xr-x.  5 apache apache    54 Nov 18 11:26 ..
drwxr-xr-x. 47 apache apache  4096 Oct  6 14:47 3rdparty
drwxr-xr-x. 50 apache apache  4096 Oct  6 14:44 apps
-rw-r--r--.  1 apache apache 19327 Oct  6 14:42 AUTHORS
drwxr-xr-x.  2 apache apache    66 Nov 21 15:18 config
-rw-r--r--.  1 apache apache  4095 Oct  6 14:42 console.php
-rw-r--r--.  1 apache apache 34520 Oct  6 14:42 COPYING
drwxr-xr-x. 23 apache apache  4096 Oct  6 14:47 core
-rw-r--r--.  1 apache apache  6317 Oct  6 14:42 cron.php
drwxrwx---.  5 apache apache   139 Nov 18 12:46 data
drwxr-xr-x.  2 apache apache  8192 Oct  6 14:42 dist
-rw-r--r--.  1 apache apache  3345 Nov 18 12:45 .htaccess
-rw-r--r--.  1 apache apache     0 Nov 18 12:19 index.html
-rw-r--r--.  1 apache apache  3456 Oct  6 14:42 index.php
drwxr-xr-x.  6 apache apache   125 Oct  6 14:42 lib
drwxr-xr-x.  2 apache apache     6 Nov 18 12:04 nextcloud
-rw-r--r--.  1 apache apache     0 Nov 18 12:19 nextcloud.log
-rw-r--r--.  1 apache apache   283 Oct  6 14:42 occ
drwxr-xr-x.  2 apache apache    23 Oct  6 14:42 ocm-provider
drwxr-xr-x.  2 apache apache    55 Oct  6 14:42 ocs
drwxr-xr-x.  2 apache apache    23 Oct  6 14:42 ocs-provider
-rw-r--r--.  1 apache apache  3139 Oct  6 14:42 public.php
-rw-r--r--.  1 apache apache  5426 Oct  6 14:42 remote.php
drwxr-xr-x.  4 apache apache   133 Oct  6 14:42 resources
-rw-r--r--.  1 apache apache    26 Oct  6 14:42 robots.txt
-rw-r--r--.  1 apache apache  2452 Oct  6 14:42 status.php
drwxr-xr-x.  3 apache apache    35 Oct  6 14:42 themes
drwxr-xr-x.  2 apache apache    43 Oct  6 14:44 updater
-rw-r--r--.  1 apache apache   101 Oct  6 14:42 .user.ini
-rw-r--r--.  1 apache apache   387 Oct  6 14:47 version.php
```



🌞 **Adapter la configuration d'Apache**

- regardez la dernière ligne du fichier de conf d'Apache pour constater qu'il existe une ligne qui inclut d'autres fichiers de conf:  
```bash
[ced@web tp2_nextcloud]$ sudo cat /etc/httpd/conf.d//nextcloud.conf
<VirtualHost *:80>
DocumentRoot /var/www/tp2_nextcloud/
ServerName  web.tp2.linux
  <Directory /var/www/tp2_nextcloud/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews
    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>
```  


🌞 **Redémarrer le service Apache** pour qu'il prenne en compte le nouveau fichier de conf:  
```bash
[ced@web]$ sudo systemctl restart httpd
[ced@web]$ sudo systemctl status httpd
     Active: active (running) since Tue 2022-11-15 12:40:01 CET; 23s ago
```

### C. Finaliser l'installation de NextCloud


🌴 **C'est chez vous ici**, baladez vous un peu sur l'interface de NextCloud, faites le tour du propriétaire :)

🌞 **Exploration de la base de données**

- connectez vous en ligne de commande à la base de données après l'installation terminée:  
```bash
[ced@db ~]$ sudo mysql -u root
Welcome to the MariaDB monitor.  Commands end with ; or \g.
```

- déterminer combien de tables ont été crées par NextCloud lors de la finalisation de l'installation
  - ***bonus points*** si la réponse à cette question est automatiquement donnée par une requête SQL

```sql
MariaDB [(none)]> use nextcloud;
Reading table information for completion of table and column names

MariaDB [(none)]> SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'nextcloud';
+----------+
| COUNT(*) |
+----------+
|       95 |
+----------+
1 row in set (0.001 sec)`
```  


