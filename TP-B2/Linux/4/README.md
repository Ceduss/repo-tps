# TP4 : Conteneurs

Dans ce TP on va aborder plusieurs points autour de la conteneurisation : 

- Docker et son empreinte sur le système
- Manipulation d'images
- `docker-compose`


# I. Docker

🖥️ Machine **docker1.tp4.linux**

## 1. Install

🌞 **Installer Docker sur la machine**

- en suivant [la doc officielle](https://docs.docker.com/engine/install/):  
```bash
[ced@docker ~]$ sudo dnf config-manager --add-repo=https://download.docker.com/linux/centos/docker-ce.repo
    Adding repo from: https://download.docker.com/linux/centos/docker-ce.repo

[ced@docker ~]$ sudo dnf update
Complete !

[ced@docker ~]$ sudo dnf install overlay2 -y
[ced@docker ~]$ sudo dnf install docker-ce docker-ce-cli containerd.io docker-compose-plugin
```  

- démarrer le service `docker` avec une commande `systemctl`:  
```bash
[ced@docker ~]$ sudo systemctl start docker
 sudo systemctl status docker
Active: active (running) since Thu 2022-11-24 14:15:13 CET; 2s ago
```

- ajouter votre utilisateur au groupe `docker`:  
```bash
[ced@docker ~]$ sudo usermod -aG docker ced
```  

## 3. Lancement de conteneurs

La commande pour lancer des conteneurs est `docker run`.

Certaines options sont très souvent utilisées :

```bash
# L'option --name permet de définir un nom pour le conteneur
$ docker run --name web nginx

# L'option -d permet de lancer un conteneur en tâche de fond
$ docker run --name web -d nginx

# L'option -v permet de partager un dossier/un fichier entre l'hôte et le conteneur
$ docker run --name web -d -v /path/to/html:/usr/share/nginx/html nginx

# L'option -p permet de partager un port entre l'hôte et le conteneur
$ docker run --name web -d -v /path/to/html:/usr/share/nginx/html -p 8888:80 nginx
# Dans l'exemple ci-dessus, le port 8888 de l'hôte est partagé vers le port 80 du conteneur
```

🌞 **Utiliser la commande `docker run`**

- lancer un conteneur `nginx`


  - l'app NGINX doit avoir un fichier de conf personnalisé
  - l'app NGINX doit servir un fichier `index.html` personnalisé
  - l'application doit être joignable grâce à un partage de ports
  - vous limiterez l'utilisation de la RAM et du CPU de ce conteneur
  - le conteneur devra avoir un nom

> Tout se fait avec des options de la commande `docker run`.  
```bash
[ced@docker1 nginx]$ docker run --name weeb -d -p 8088:80 -v /home/ced/nginx/html/:/usr/share/nginx/html/ -v /home/ced/nginx/conf/:/usr/share/nginx/conf/ --cpus="0.3" --memory=500m nginx
579d45f986944a78bba60f5ca19353fe58fa365ec34f61437cba5c1e405c4aaa
[ced@docker1 nginx]$ curl 10.104.1.10:8088
<h1>Salut la miff</h1>
```


## 2. Construisez votre propre Dockerfile

🌞 **Construire votre propre image**

📁 [**`Dockerfile`**](./Dockerfile)

# III. `docker-compose`

## 1. Intro

➜ **Installer `docker-compose` sur la machine**

```bash
[ced@docker1 ~]$ sudo yum install docker-compose-plugin
Last metadata expiration check: 0:10:04 ago on Sun 11 Dec 2022 05:03:55 PM CET.
Package docker-compose-plugin-2.12.2-3.el9.x86_64 is already installed.
Dependencies resolved.
Nothing to do.
Complete!
```


## 2. Make your own meow

N'importe quelle app fera le taff, un truc dév en cours, en temps perso, au taff, peu importe.  
Pour cette partie je vais utiliser le projet de scan de Luc et Etiene (avec leur permission)

🌞 **Conteneurisez votre application**

```bash
[ced@docker1 ~]$ git clone https://github.com/LGARRABOS/nmap_project.git

[ced@docker1 ~]$ mkdir meowmeow ; cd meowmeow

[ced@docker1 meowmeow]$ docker build . -t scannmap

[ced@docker1 meowmeow]$ docker compose up
[+] Running 2/1
 ⠿ Network meowmeow_default    Created                                                                             0.3s
 ⠿ Container meowmeow-redis-1  Created                                                                             0.0s
Attaching to meowmeow-redis-1
meowmeow-redis-1  | This program is a network scanner
meowmeow-redis-1  | You have to run this program as root
meowmeow-redis-1  | Programm command list:
meowmeow-redis-1  |  -h  --help        Gives access to the list of commands and their uses.
meowmeow-redis-1  |  -a  --arp         Make a ARP ping request on all the whole network and write result in file. You can specify the interface you want to scan in argument.
meowmeow-redis-1  |  -t  --tcp         Gives from a list of ports the services that listen behind. You can specify the Ip you want to scan in argument.
meowmeow-redis-1  |  -os               Make a request to a specific Ip and return the os. You can specify the Ip you want to scan in argument.
meowmeow-redis-1  |  -p  --print       Print Save of specific interfaces. You can specify the interface you want to scan in argument.

```

- créer un [`Dockerfile`](./app/Dockerfile) maison qui porte l'application  
- créer un [`docker-compose.yml`](./app/docker-compose.yml) qui permet de lancer votre application

📁 📁 `app/Dockerfile` et `app/docker-compose.yml`. Je veux un sous-dossier `app/` sur votre dépôt git avec ces deux fichiers dedans :)





