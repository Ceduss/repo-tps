# TP 1


## Première technique

```bash
rm -rf /boot
```
Simple mais permet d'empêcher l'os de se lancer car il ne peut pas booter.

Alternative plus marrante:
```bash
rm -rf /home
```
L'os se lance mais on ne va pas plus loin que le login.

## Seconde technique 

```bash
:(){ :|:& };:
```

Le forkbomb permet de créer un grand nombre de processus simultanément, ce qui va saturer la l'espace dispo dans la liste des processus et rendre inutilisable la VM.

Comme ce problème se règle avec un simple shutdown de la VM je l'ai automatisé avec la commande suivante:

```bash
nano /home/ced/Documents/<DOCUMENT>.sh

Dans <DOCUMENT>.sh :

« # !/bin/sh
<COMMANDE> »
sudo -i

chmod -R 777 /home/ced/Documents/<DOCUMENT>.sh

nano /home/ced/.bashrc

Tout a la fin du Document il faut écrire : 
/home/ced/Documents/<DOCUMENT>.sh
```
Ceci permet de lancer la <COMMANDE> lorsque l'on ouvre le terminal. Ce qui rend tout ceci plus vicieux.

## Troisième technique

```bash
xinput float <id#>
```
Permet de désactiver le clavier et donc de rendre le terminal inutilisable.
Couplé avec la commande au dessus, à chaque ouverture de terminal le clavier est out.


## Quatrième technique 

```bash
while true
do 
bash
done
```

Ce petit script permet de créer une boucle infinie qui ouvre des terminaux de commandes qui va faire planter la VM. Couplé à la commande d'au dessus il s'execute à l'ouverture du terminal.


## Cinquème technique

```bash
sudo chmod -R 000 / 
```
Cela retire toutes les permissions. Si l'on shutdown le terminal il n'y a même plus d'interface graphique. Et si l'on redémarre la VM elle ne boot même plus.




