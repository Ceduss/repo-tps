
🌞 **Choisissez et définissez une IP à la VM**


```bash
[ced@node1 ~]$ sudo cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
[sudo] password for ced:
TYPE=Ethernet
BOOTPROTO=static
NAME=enp0s8
DEVICE=enp0s8
ONBOOT=yes
IPADDR=10.250.1.50
NETMASK=255.255.255.0

[ced@node1 ~]$ sudo nmcli con reload

[ced@node1 ~]$ sudo nmcli con up enp0s8

[ced@node1 ~]$ ip a
[...]
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:d3:b4:8c brd ff:ff:ff:ff:ff:ff
    inet 10.250.1.50/24 brd 10.250.1.255 scope global noprefixroute enp0s8
[...]
```


➜ **Connexion SSH fonctionnelle**


🌞 **Vous me prouverez que :**

- le service ssh est actif sur la VM
```bash
[ced@node1 ~]$ sudo systemctl status sshd
● sshd.service - OpenSSH server daemon
   Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor preset: enabled)
   Active: active (running) since Wed 2021-11-24 19:52:47 CET; 5min ago
     Docs: man:sshd(8)
           man:sshd_config(5)
 Main PID: 857 (sshd)
    Tasks: 1 (limit: 4946)
```
- vous pouvez vous connecter à la VM, grâce à un échange de clés
```bash
[ced@node1 ~]$ cat /home/ced/.ssh/authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC//s2fTmsDmxZts1cGeFOkTTM+ixVwcPKfZJPW4Z1QvGY6pnHr4zqboyNDa4rVxEXIbwKk0LRwddDIgiq5vtmUFjqqS08ssQcH37ADudda0SW6nksH1UDaERt7HaH3AQHhkt94qzq1ihO2+vxSytAizzTvasDNO6fSReSXiy2akVsbfQrTIItcetKjEjql4rbEID5/GxWrQa8VzJqyTMtU750tFJx8uDDjThsgq6iysGtdIRaheoRfNV7c9VSnes4ZJMgOfCBPsdm49+CYVkO6ztDkC69EP9nCnYBbKaibfieBeupjvjats6L3HS1FPwuaf0RB8k9V9kStzG7WorY1bXuJGNuDqHPDZLDCuGk27ZrmRUGLODDm6d2Gn4bwyO8WDmsCcUK7KMjZkmIbTMFp6Fuck83dQ6hVZcZRnVAlKGW02SVf0uSCwYGaioH+dVfK2gdhyxJoflkQEurxHCle/lHAHGfWgXt6cUWjQ6DOqppKab7rTjGA00Hq6GU0YJXcUbH36DkbhSrUl47PUgMFUQqT68CxmBEwltzOx71oLCKxxBCwQIEtVzyLb6GV9BCSCBpd0/AhDE0UHG6kt4SnLsshBXkAO/oQJJKPE3fByKWuKGqIvoiguiMxhU3UZaL8UU8yWW7INdyf/oV0LAOcZGPE0a+cM+tNcFd7ZtJRxw== vidal@DESKTOP-294MJTQ

PS C:\Users\vidal> cat .\.ssh\known_hosts
#J'ai préféré utiliser known_hosts car on voit qu'il y a eu une connexion à l'ip 10.250.1.50
10.250.1.50 ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDLhdWEadqxwnKudqg1blSBu6rcKfVQ81y2uMhwn1rjJHfm0JYPM1COmHyVRA7vvG6d5DxOTvF0Y8cvym0wCwlmHjtd5mqKNomzlT9XZgGS6jrgWKxdUk5w4UkdOiposnWwQrQlwfHawWo4rHQX8YUhl4JyYNEmZWr5iSw+vRK8s6GqVv78dh6MOoembeGjYT7Jx+Y4KiHRw36eNI6ZOYapZ9pdm7FDOK5LnGK4Y3IWkqikx7iYdcujkhk2emJhCeabaNpyHu7hi2zm0PmboceUf23Lf2FH3MHwT8Mc0VTFkArrhmP95nizFkHFaj2D3Cj8kpuMUWPDZde0oOeUqekzSC61bMuICcQyiuypeLHANCLIP66pbE3VW0+ZHIPTezOmh1hYa8qbzhrCglPaASJgoHiU7HrhKRRy2MeR9tE2sOZpRKj9s0HxLpSfXOTz4PweZ9aeHRORlJPcWaKdXjnca8pyjVIHRKWro0MYef6Bi6dkbFuUO3j8DKQfjkzikHk=

PS C:\Users\vidal> ssh ced@10.250.1.50
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Wed Nov 24 20:29:25 2021 from 10.250.1.1
[ced@node1 ~]$
```

➜ **Accès internet**

🌞 **Prouvez que vous avez un accès internet**

- avec une commande `ping`
```bash
[ced@node1 ~]$ ping 1.1.1.1
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from 1.1.1.1: icmp_seq=1 ttl=55 time=17.2 ms
64 bytes from 1.1.1.1: icmp_seq=2 ttl=55 time=17.2 ms
64 bytes from 1.1.1.1: icmp_seq=3 ttl=55 time=17.3 ms
--- 1.1.1.1 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2003ms
rtt min/avg/max/mdev = 17.161/17.227/17.277/0.159 ms
```

🌞 **Prouvez que vous avez de la résolution de nom**

```bash
[ced@node1 ~]$ ping cloudflare.com
PING cloudflare.com (104.16.132.229) 56(84) bytes of data.
64 bytes from 104.16.132.229 (104.16.132.229): icmp_seq=1 ttl=55 time=17.3 ms
64 bytes from 104.16.132.229 (104.16.132.229): icmp_seq=2 ttl=55 time=18.8 ms
64 bytes from 104.16.132.229 (104.16.132.229): icmp_seq=3 ttl=55 time=17.4 ms
^C
--- cloudflare.com ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2005ms
rtt min/avg/max/mdev = 17.250/17.828/18.800/0.691 ms
```

---

➜ **Nommage de la machine**

🌞 **Définissez `node1.tp4.linux` comme nom à la machine**

- montrez moi le contenu du fichier `/etc/hostname`
```bash
[ced@node1 ~]$ cat /etc/hostname
node1.tp2.linux
```
- tapez la commande `hostname` (sans argument ni option) pour afficher votre hostname actuel
```bash
[ced@node1 ~]$ hostname
node1.tp4.linux
```

# III. Mettre en place un service

## 2. Install

🌞 **Installez NGINX en vous référant à des docs online**
```bash
[ced@node1 ~]$ sudo dnf install nginx
Complete!

[ced@node1 ~]$ sudo systemctl start nginx

[ced@node1 ~]$ sudo systemctl enable nginx
Created symlink /etc/systemd/system/multi-user.target.wants/nginx.service → /usr/lib/systemd/system/nginx.service.

[ced@node1 ~]$ nginx -v
nginx version: nginx/1.14.1
```

## 3. Analyse



Commencez donc par démarrer le service NGINX :

```bash
[ced@node1 ~]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-11-24 21:20:26 CET; 9s ago
  Process: 4428 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
  Process: 4427 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
  Process: 4425 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)
 Main PID: 4430 (nginx)
    Tasks: 2 (limit: 4946)
   Memory: 6.7M
```

🌞 **Analysez le service NGINX**

- avec une commande `ps`, déterminer sous quel utilisateur tourne le processus du service NGINX
```bash
[ced@node1 ~]$ ps -ef | grep -F nginx
root        4430       1  0 21:20 ?        00:00:00 nginx: master process /usr/sbin/nginx
nginx       4431    4430  0 21:20 ?        00:00:00 nginx: worker process
ced         4487    1802  0 21:39 pts/0    00:00:00 grep --color=auto -F nginx
```
- avec une commande `ss`, déterminer derrière quel port écoute actuellement le serveur web
```bash
[ced@node1 ~]$ sudo ss -lntp | grep nginx
LISTEN 0      128          0.0.0.0:80        0.0.0.0:*    users:(("nginx",pid=4431,fd=8),("nginx",pid=4430,fd=8))
LISTEN 0      128             [::]:80           [::]:*    users:(("nginx",pid=4431,fd=9),("nginx",pid=4430,fd=9))
```
- en regardant la conf, déterminer dans quel dossier se trouve la racine web

```bash
[ced@node1 sbin]$ cat /etc/nginx/nginx.conf
    server {
        listen       80 default_server;
        listen       [::]:80 default_server;
        server_name  _;
        root         /usr/share/nginx/html;

[ced@node1 ~]$ cd /usr/share/nginx/html
[ced@node1 html]$ ls
404.html  50x.html  index.html  nginx-logo.png  poweredby.png
```
- inspectez les fichiers de la racine web, et vérifier qu'ils sont bien accessibles en lecture par l'utilisateur qui lance le processus

```bash
[ced@node1 html]$ ls -al
total 20
drwxr-xr-x. 2 root root   99 Nov 24 21:19 .
drwxr-xr-x. 4 root root   33 Nov 24 21:19 ..
-rw-r--r--. 1 root root 3332 Jun 10 11:09 404.html
-rw-r--r--. 1 root root 3404 Jun 10 11:09 50x.html
-rw-r--r--. 1 root root 3429 Jun 10 11:09 index.html
-rw-r--r--. 1 root root  368 Jun 10 11:09 nginx-logo.png
-rw-r--r--. 1 root root 1800 Jun 10 11:09 poweredby.png
```

## 4. Visite du service web

🌞 **Configurez le firewall pour autoriser le trafic vers le service NGINX** 
```bash
[ced@node1 html]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[ced@node1 html]$ sudo firewall-cmd --reload
success
```

🌞 **Tester le bon fonctionnement du service**

```bash
[ced@node1 html]$ curl http://10.250.1.50:80
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
  <head>
    <title>Test Page for the Nginx HTTP Server on Rocky Linux</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <style type="text/css">
      /*<![CDATA[*/
      body {
        background-color: #fff;
        color: #000;
        font-size: 0.9em;
        font-family: sans-serif, helvetica;
        margin: 0;
        padding: 0;
      }
      :link {
        color: #c00;
      }
      :visited {
        color: #c00;
      }
      a:hover {
        color: #f50;
      }
      h1 {
        text-align: center;
        margin: 0;
        padding: 0.6em 2em 0.4em;
        background-color: #10B981;
        color: #fff;
        font-weight: normal;
        font-size: 1.75em;
        border-bottom: 2px solid #000;
      }
      h1 strong {
        font-weight: bold;
        font-size: 1.5em;
      }
      h2 {
        text-align: center;
        background-color: #10B981;
        font-size: 1.1em;
        font-weight: bold;
        color: #fff;
        margin: 0;
        padding: 0.5em;
        border-bottom: 2px solid #000;
      }
      hr {
        display: none;
      }
      .content {
        padding: 1em 5em;
      }
      .alert {
        border: 2px solid #000;
      }

      img {
        border: 2px solid #fff;
        padding: 2px;
        margin: 2px;
      }
      a:hover img {
        border: 2px solid #294172;
      }
      .logos {
        margin: 1em;
        text-align: center;
      }
      /*]]>*/
    </style>
  </head>

  <body>
    <h1>Welcome to <strong>nginx</strong> on Rocky Linux!</h1>

    <div class="content">
      <p>
        This page is used to test the proper operation of the
        <strong>nginx</strong> HTTP server after it has been installed. If you
        can read this page, it means that the web server installed at this site
        is working properly.
      </p>

      <div class="alert">
        <h2>Website Administrator</h2>
        <div class="content">
          <p>
            This is the default <tt>index.html</tt> page that is distributed
            with <strong>nginx</strong> on Rocky Linux. It is located in
            <tt>/usr/share/nginx/html</tt>.
          </p>

          <p>
            You should now put your content in a location of your choice and
            edit the <tt>root</tt> configuration directive in the
            <strong>nginx</strong>
            configuration file
            <tt>/etc/nginx/nginx.conf</tt>.
          </p>

          <p>
            For information on Rocky Linux, please visit the
            <a href="https://www.rockylinux.org/">Rocky Linux website</a>. The
            documentation for Rocky Linux is
            <a href="https://www.rockylinux.org/"
              >available on the Rocky Linux website</a
            >.
          </p>
        </div>
      </div>

      <div class="logos">
        <a href="http://nginx.net/"
          ><img
            src="nginx-logo.png"
            alt="[ Powered by nginx ]"
            width="121"
            height="32"
        /></a>
        <a href="http://www.rockylinux.org/"><img
            src="poweredby.png"
            alt="[ Powered by Rocky Linux ]"
            width="88" height="31" /></a>

      </div>
    </div>
  </body>
</html>
```
## 5. Modif de la conf du serveur web

🌞 **Changer le port d'écoute**

- faites écouter NGINX sur le port 8080
```bash
[ced@node1 html]$ sudo cat /etc/nginx/nginx.conf
   server {
        listen       8080 default_server;
        listen       [::]:8080 default_server;
        server_name  _;
        root         /usr/share/nginx/html;
```
- redémarrer le service pour que le changement prenne effet
```bash
[ced@node1 ~]$ sudo systemctl restart nginx

[ced@node1 ~]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-11-24 22:37:42 CET; 2min 6s ago
  Process: 4619 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
  Process: 4618 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
  Process: 4615 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)
 Main PID: 4621 (nginx)
    Tasks: 2 (limit: 4946)
```
- prouvez-moi que le changement a pris effet avec une commande `ss`
```bash
[ced@node1 ~]$ sudo ss -lntp | grep nginx
LISTEN 0      128          0.0.0.0:8080      0.0.0.0:*    users:(("nginx",pid=4622,fd=8),("nginx",pid=4621,fd=8))
LISTEN 0      128             [::]:8080         [::]:*    users:(("nginx",pid=4622,fd=9),("nginx",pid=4621,fd=9))
```
- n'oubliez pas de fermer l'ancier port dans le firewall, et d'ouvrir le nouveau
```bash
[ced@node1 ~]$ sudo firewall-cmd --remove-port=80/tcp --permanent
success
[ced@node1 ~]$ sudo firewall-cmd --add-port=8080/tcp --permanent
success
[ced@node1 ~]$ sudo firewall-cmd --reload
success
```

- prouvez avec une commande `curl` sur votre machine que vous pouvez désormais visiter le port 8080
```bash
[ced@node1 ~]$ curl http://10.250.1.50:8080
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
  <head>
    <title>Test Page for the Nginx HTTP Server on Rocky Linux</title>
```
---

🌞 **Changer l'utilisateur qui lance le service**

- pour ça, vous créerez vous-même un nouvel utilisateur sur le système : `web`
```bash
[ced@node1 ~]$ sudo useradd Web -m -s /bin/sh -u 2002 -p web

[ced@node1 ~]$ cat /etc/passwd | grep Web
Web:x:2002:2002::/home/Web:/bin/sh
```

- un peu de conf à modifier dans le fichier de conf de NGINX pour définir le nouvel utilisateur en tant que celui qui lance le service
```bash
[ced@node1 ~]$ cat /etc/nginx/nginx.conf | grep Web
user Web;

[ced@node1 ~]$ sudo systemctl restart nginx
```

- vous prouverez avec une commande `ps` que le service tourne bien sous ce nouveau utilisateur
```bash
[ced@node1 ~]$ ps -ef  | grep nginx
root        4986       1  0 23:14 ?        00:00:00 nginx: master process /usr/sbin/nginx
Web         4987    4986  0 23:14 ?        00:00:00 nginx: worker process
ced         4995    1802  0 23:15 pts/0    00:00:00 grep --color=auto nginx
```

---

🌞 **Changer l'emplacement de la racine Web**

- vous créerez un nouveau dossier : `/var/www/super_site_web`
```bash
[ced@node1 ~]$ sudo mkdir /var/www
[ced@node1 ~]$ sudo mkdir /var/www/super_site_web
```
- avec un fichier  `/var/www/super_site_web/index.html` qui contient deux trois lignes de HTML, peu importe, un bon `<h1>toto</h1>` des familles, ça fera l'affaire
```bash
[ced@node1 ~]$ sudo nano /var/www/super_site_web/index.html
```

- le dossier et tout son contenu doivent appartenir à `web`
```bash
[ced@node1 ~]$ sudo chown Web /var/www -R

[ced@node1 ~]$ ls -al /var/www
total 4
drwxr-xr-x.  3 Web  root   28 Nov 24 23:17 .
drwxr-xr-x. 22 root root 4096 Nov 24 23:16 ..
drwxr-xr-x.  2 Web  root   24 Nov 24 23:20 super_site_web
```
- configurez NGINX pour qu'il utilise cette nouvelle racine web
```bash
[ced@node1 ~]$ sudo cat /var/www/super_site_web/index.html 
  server {
        listen       8080 default_server;
        listen       [::]:8080 default_server;
        server_name  _;
        root         /var/www/super_site_web/index.html;
```
- n'oubliez pas de redémarrer le service pour que le changement prenne effet
```bash
[ced@node1 ~]$ sudo systemctl restart nginx
```
- prouvez avec un `curl` depuis votre hôte que vous accédez bien au nouveau site
```bash
[ced@node1 ~]$ curl http://10.250.1.50:8080
<h1>Le plus bô des sites Web <3 </h1>

<p>Tu es vraiment très beau aujourd'hui tu le sais ça? :D </p>
```