# I. Setup DB


## Sommaire

- [I. Setup DB](#i-setup-db)
  - [Sommaire](#sommaire)
  - [1. Install MariaDB](#1-install-mariadb)
  - [2. Conf MariaDB](#2-conf-mariadb)
  - [3. Test](#3-test)

## 1. Install MariaDB


🌞 **Installer MariaDB sur la machine `db.tp5.linux`**

- le paquet s'appelle `mariadb-server`
```bash
[ced@db ~]$ sudo dnf install mariadb-server
Complete!
```

🌞 **Le service MariaDB**

- lancez-le avec une commande `systemctl`
```bash
[ced@db ~]$ sudo systemctl start mariadb
```

- exécutez la commande `sudo systemctl enable mariadb` pour faire en sorte que MariaDB se lance au démarrage de la machine
```bash
[ced@db ~]$ sudo systemctl enable mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.
```

- vérifiez qu'il est bien actif avec une commande `systemctl`
```bash
[ced@db ~]$ sudo systemctl status mariadb
● mariadb.service - MariaDB 10.3 database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; vendor preset: disabled)
   Active: active (running) since Sat 2021-11-27 17:39:35 CET; 4min 50s ago
```

- déterminer sur quel port la base de données écoute avec une commande `ss`
```bash
[ced@db ~]$ sudo ss -lptn
State   Recv-Q  Send-Q    Local Address:Port     Peer Address:Port  Process
LISTEN  0       80                    *:3306                *:*      users:(("mysqld",pid=4999,fd=21))
```

- déterminez sous quel utilisateur est lancé le process MariaDB
```bash
[ced@db ~]$ ps -ef | grep mariadb
ced         5814    5783  0 20:48 pts/0    00:00:00 grep --color=auto mariadb

[ced@db ~]$ ps -ef | grep mysql
mysql       4999       1  0 Nov27 ?        00:00:15 /usr/libexec/mysqld --basedir=/usr
ced         5817    5783  0 20:50 pts/0    00:00:00 grep --color=auto mysql
```

🌞 **Firewall**

- ouvrez le port utilisé par MySQL à l'aide d'une commande `firewall-cmd`
```bash
[ced@db ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent
success

[ced@db ~]$ sudo firewall-cmd --reload
success
```


## 2. Conf MariaDB

Première étape : le `mysql_secure_installation`. C'est un binaire qui sert à effectuer des configurations très récurrentes, on fait ça sur toutes les bases de données à l'install.  
C'est une question de sécu.

🌞 **Configuration élémentaire de la base**

- exécutez la commande `mysql_secure_installation`
```bash
[ced@db ~]$ mysql_secure_installation

NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

# La question est simple: faut-il un mdp pour utiliser root.
Set root password? [Y/n] y
New password:
Re-enter new password:
Password updated successfully!
Reloading privilege tables..
 ... Success!
# Etant donné ue root a tous les droits, je préfère set un mdp fort pour eviter que un lambda accède a root et foute le dawa, c'est mieux :)


# MariaDb autorise par défaut que des anonymes (qui n'ont pas de compte users) à se co au serveur. Il est simplement demandé ici si l'on garde ou non cette option. 
Remove anonymous users? [Y/n] Y
 ... Success!
# Je ne souhaite pas qu'un anonyme puisse ne serait qu'envisager de se connecter à mon serveur je retire donc cette option. De plus cela me permet de savoir exactement qui fais quoi, qd et comment.


# Faut-il pouvoir utiliser le users root à distance?
Disallow root login remotely? [Y/n] Y
 ... skipping.
# Hors de question de pouvoir root à distance, si l'on veux utiliser ce users on se déplace et on utilise la machine "maitresse" (ici mon pc) certes c'est relou mais ca évite des emmerdes :)


# Mariadb a par défaut une base de donnée "test" qui est accessible par tout le monde.
Remove test database and access to it? [Y/n] y
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!
# Accessible par tout le monde = nop direct. D'autant plus que je ne compte pas m'en servir vu que je vais directement l'utiliser pour le tp donc je n'en ai nullement besoin.


# Basiquement on nous demande si l'on veut appliquer les changements de suite.
Reload privilege tables now? [Y/n] Y
 ... Success!
# Il vaut mieux, cela évite les oublis et au moins comme ça c'est fait.
Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!
```

> Il existe des tonnes de guides sur internet pour expliquer ce que fait cette commande et comment répondre aux questions, afin d'augmenter le niveau de sécurité de la base.

> Personnellement j'ai juste essayé de comprendre à qupi tout ceci rimait sans chercher de ressources sur internet, si je suis pas trop bête cela devrait être plus ou moins correct. 

---

🌞 **Préparation de la base en vue de l'utilisation par NextCloud**

- pour ça, il faut vous connecter à la base

```bash
[ced@db ~]$ sudo mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 22
Server version: 10.3.28-MariaDB MariaDB Server
```

Puis, dans l'invite de commande SQL :

```sql
MariaDB [(none)]> CREATE USER 'nextcloud'@'10.5.1.11' IDENTIFIED BY 'meow';
Query OK, 0 rows affected (0.000 sec)

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.000 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.5.1.11';
Query OK, 0 rows affected (0.000 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.000 sec)
```

## 3. Test


🌞 **Installez sur la machine `web.tp5.linux` la commande `mysql`**

- vous utiliserez la commande `dnf provides` pour trouver dans quel paquet se trouve cette commande

```bash
[ced@web ~]$ sudo dnf provides mysql
[sudo] password for ced:
Last metadata expiration check: 1 day, 7:21:16 ago on Sat 27 Nov 2021 04:03:17 PM CET.
mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64 : MySQL client programs and shared libraries
Repo        : appstream
Matched from:
Provide    : mysql = 8.0.26-1.module+el8.4.0+652+6de068a7
```

```bash
[ced@web ~]$ sudo dnf install mysql
Installed:
  mariadb-connector-c-config-3.1.11-2.el8_3.noarch               mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64
  mysql-common-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64

Complete!
```

🌞 **Tester la connexion**

- utilisez la commande `mysql` depuis `web.tp5.linux` pour vous connecter à la base qui tourne sur `db.tp5.linux`
```bash
[ced@web ~]$ mysql --host=10.5.1.12 -P 3306 --user=nextcloud -p -D nextcloud
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 11
Server version: 5.5.5-10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2021, Oracle and/or its affiliates.
```
- une fois connecté à la base en tant que l'utilisateur `nextcloud` :
```sql
# la base de données est vide
mysql> SHOW TABLES
    ->
    ->
```


---


# II. Setup Web

## Sommaire

- [II. Setup Web](#ii-setup-web)
  - [Sommaire](#sommaire)
  - [1. Install Apache](#1-install-apache)
    - [A. Apache](#a-apache)
    - [B. PHP](#b-php)
  - [2. Conf Apache](#2-conf-apache)
  - [3. Install NextCloud](#3-install-nextcloud)
  - [4. Test](#4-test)

## 1. Install Apache

### A. Apache

🌞 **Installer Apache sur la machine `web.tp5.linux`**

- le paquet qui contient Apache s'appelle `httpd`
```bash
[ced@web ~]$ sudo dnf install httpd
Last metadata expiration check: 1 day, 7:39:07 ago on Sat 27 Nov 2021 04:03:17 PM CET.

Installed:
  apr-1.6.3-12.el8.x86_64                                          apr-util-1.6.1-6.el8.1.x86_64
  apr-util-bdb-1.6.1-6.el8.1.x86_64                                apr-util-openssl-1.6.1-6.el8.1.x86_64
  httpd-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64               httpd-filesystem-2.4.37-43.module+el8.5.0+714+5ec56ee8.noarch
  httpd-tools-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64         mod_http2-1.15.7-3.module+el8.5.0+695+1fa8055e.x86_64
  rocky-logos-httpd-85.0-3.el8.noarch

Complete!
```
- le service aussi s'appelle `httpd`
```bash
[ced@web ~]$ sudo systemctl start httpd

[ced@web ~]$ systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; disabled; vendor preset: disabled)
   Active: active (running) since Sun 2021-11-28 23:53:30 CET; 56s ago
     Docs: man:httpd.service(8)
 Main PID: 2407 (httpd)
   Status: "Running, listening on: port 80"
```

---

🌞 **Analyse du service Apache**

- lancez le service `httpd` et activez le au démarrage
```bash
[ced@web ~]$ sudo systemctl enable httpd
[ced@web ~]$ sudo systemctl start httpd
[ced@web ~]$ systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Mon 2021-11-29 04:53:48 CET; 33s ago
     Docs: man:httpd.service(8)
 Main PID: 1548 (httpd)
   Status: "Running, listening on: port 80"
    Tasks: 213 (limit: 4946)
   Memory: 28.9M
```

- isolez les processus liés au service `httpd`
```bash
[ced@web ~]$ ps -ef | grep httpd
root        1548       1  0 04:53 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1549    1548  0 04:53 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1550    1548  0 04:53 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1551    1548  0 04:53 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1552    1548  0 04:53 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
ced         1794    1476  0 04:55 pts/0    00:00:00 grep --color=auto httpd
```
- déterminez sur quel port écoute Apache par défaut
```bash
[ced@web ~]$ ss -lnpt
LISTEN     0          128                          *:80                        *:*
```
- déterminez sous quel utilisateur sont lancés les processus Apache
>voir ci-dessus

---

🌞 **Un premier test**

- ouvrez le port d'Apache dans le firewall
```bash
[ced@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success

[ced@web ~]$ sudo firewall-cmd --reload
success
```
- testez, depuis votre PC, que vous pouvez accéder à la page d'accueil par défaut d'Apache
```bash
[ced@web ~]$ curl http://10.5.1.11
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/


 </head>
  <body>
    <h1>HTTP Server <strong>Test Page</strong></h1>

    <div class='row'>

      <div class='col-sm-12 col-md-6 col-md-6 '></div>
          <p class="summary">This page is used to test the proper operation of
            an HTTP server after it has been installed on a Rocky Linux system.
            If you can read this page, it means that the software it working
            correctly.</p>
      </div>

      <div class='col-sm-12 col-md-6 col-md-6 col-md-offset-12'>


        <div class='section'>
          <h2>Just visiting?</h2>

          <p>This website you are visiting is either experiencing problems or
          could be going through maintenance.</p>

          <p>If you would like the let the administrators of this website know
          that you've seen this page instead of the page you've expected, you
          should send them an email. In general, mail sent to the name
          "webmaster" and directed to the website's domain should reach the
          appropriate person.</p>
```

### B. PHP


🌞 **Installer PHP**

```bash
[ced@web ~]$ sudo dnf install epel-release
Downloading Packages:
epel-release-8-13.el8.noarch.rpm
Installed:
  epel-release-8-13.el8.noarch

Complete!

[ced@web ~]$ sudo dnf update
Extra Packages for Enterprise Linux 8 - x86_64                        3.0 MB/s |  11 MB     00:03
Extra Packages for Enterprise Linux Modular 8 - x86_64                779 kB/s | 958 kB     00:01
Last metadata expiration check: -1 day, 18:08:23 ago on Mon 29 Nov 2021 11:19:03 AM CET.
Dependencies resolved.
Nothing to do.
Complete!

[ced@web ~]$ sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
Installed:
  remi-release-8.5-1.el8.remi.noarch

Complete!

[ced@web ~]$ sudo dnf module enable php:remi-7.4
Complete!


[ced@web ~]$ sudo dnf install zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp

Installed:
  environment-modules-4.5.2-1.el8.x86_64             gd-2.2.5-7.el8.x86_64
  jbigkit-libs-2.1-14.el8.x86_64                     libXpm-3.5.12-8.el8.x86_64
  libicu69-69.1-1.el8.remi.x86_64                    libjpeg-turbo-1.5.3-12.el8.x86_64
  libsodium-1.0.18-2.el8.x86_64                      libtiff-4.0.9-20.el8.x86_64
  libwebp-1.0.0-5.el8.x86_64                         oniguruma5php-6.9.7.1-1.el8.remi.x86_64
  php74-libzip-1.8.0-1.el8.remi.x86_64               php74-php-7.4.26-1.el8.remi.x86_64
  php74-php-bcmath-7.4.26-1.el8.remi.x86_64          php74-php-cli-7.4.26-1.el8.remi.x86_64
  php74-php-common-7.4.26-1.el8.remi.x86_64          php74-php-fpm-7.4.26-1.el8.remi.x86_64
  php74-php-gd-7.4.26-1.el8.remi.x86_64              php74-php-gmp-7.4.26-1.el8.remi.x86_64
  php74-php-intl-7.4.26-1.el8.remi.x86_64            php74-php-json-7.4.26-1.el8.remi.x86_64
  php74-php-mbstring-7.4.26-1.el8.remi.x86_64        php74-php-mysqlnd-7.4.26-1.el8.remi.x86_64
  php74-php-opcache-7.4.26-1.el8.remi.x86_64         php74-php-pdo-7.4.26-1.el8.remi.x86_64
  php74-php-pecl-zip-1.20.0-1.el8.remi.x86_64        php74-php-process-7.4.26-1.el8.remi.x86_64
  php74-php-sodium-7.4.26-1.el8.remi.x86_64          php74-php-xml-7.4.26-1.el8.remi.x86_64
  php74-runtime-1.0-3.el8.remi.x86_64                scl-utils-1:2.0.2-14.el8.x86_64
  tcl-1:8.6.8-2.el8.x86_64

Complete!
```

## 2. Conf Apache

🌞 **Analyser la conf Apache**

- mettez en évidence, dans le fichier de conf principal d'Apache, la ligne qui inclut tout ce qu'il y a dans le dossier de *drop-in*
```bash
[ced@web ~]$ cat /etc/httpd/conf/httpd.conf | grep Include
Include conf.modules.d/*.conf

IncludeOptional conf.d/*.conf
```

🌞 **Créer un VirtualHost qui accueillera NextCloud**

- créez un nouveau fichier dans le dossier de *drop-in*
```bash
[ced@web ~]$ sudo nano /etc/httpd/conf.d/VHost.conf

[ced@web ~]$ cat /etc/httpd/conf.d/VHost.conf

<VirtualHost *:80>
  DocumentRoot /var/www/nextcloud/html/  

  ServerName  web.tp5.linux  

  <Directory /var/www/nextcloud/html/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews

    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>
```

🌞 **Configurer la racine web**

- la racine Web, on l'a configurée dans Apache pour être le dossier `/var/www/nextcloud/html/`
- creéz ce dossier
```bash
[ced@web ~]$ sudo mkdir -p /var/www/nextcloud/html/
```

- faites appartenir le dossier et son contenu à l'utilisateur qui lance Apache 
```bash
[ced@web ~]$ sudo chown apache /var/www/nextcloud/

[ced@web ~]$ ls -al /var/www/nextcloud/
total 0
drwxr-xr-x. 3 apache root 18 Nov 29 06:07 .
drwxr-xr-x. 5 root   root 50 Nov 29 06:07 ..
drwxr-xr-x. 2 apache root  6 Nov 29 06:07 html
[ced@web ~]$
```


🌞 **Configurer PHP**


- pour récupérer la timezone actuelle de la machine, utilisez la commande `timedatectl` 
```bash
[ced@web ~]$ timedatectl
               Local time: Mon 2021-11-29 06:18:03 CET
           Universal time: Mon 2021-11-29 05:18:03 UTC
                 RTC time: Mon 2021-11-29 05:18:03
                Time zone: Europe/Paris (CET, +0100)
```

- modifiez le fichier `/etc/opt/remi/php74/php.ini` :
```bash
[ced@web ~]$ sudo nano /etc/opt/remi/php74/php.ini

[ced@web ~]$ sudo cat /etc/opt/remi/php74/php.ini | grep date.timezone
; http://php.net/date.timezone
;date.timezone = "Europe/Paris"
```

## 3. Install NextCloud


🌞 **Récupérer Nextcloud**

```bash
[ced@web ~]$ curl -SLO https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  148M  100  148M    0     0  1186k      0  0:02:07  0:02:07 --:--:-- 1216k
[ced@web ~]$ ls
nextcloud-21.0.1.zip
```

🌞 **Ranger la chambre**

- extraire le contenu de NextCloud (beh ui on a récup un `.zip`)
```bash
[ced@web ~]$ unzip nextcloud-21.0.1.zip

[ced@web ~]$ ls -a
.  ..  .bash_history  .bash_logout  .bash_profile  .bashrc  nextcloud  nextcloud-21.0.1.zip  .ssh
```
- déplacer tout le contenu dans la racine Web
```bash
[ced@web ~]$ sudo mv nextcloud /var/www/nextcloud/html/

[ced@web ~]$ ls -a
.  ..  .bash_history  .bash_logout  .bash_profile  .bashrc  nextcloud-21.0.1.zip  .ssh

[ced@web ~]$ sudo rm nextcloud-21.0.1.zip

[ced@web ~]$ ls -a
.  ..  .bash_history  .bash_logout  .bash_profile  .bashrc  .ssh
```

- n'oubliez pas de gérer les permissions de tous les fichiers déplacés 
```bash
[ced@web ~]$ sudo chown -R apache /var/www/nextcloud/

[ced@web ~]$ ls -al /var/www/nextcloud/
total 0
drwxr-xr-x. 3 apache root 18 Nov 29 06:07 .
drwxr-xr-x. 5 root   root 50 Nov 29 06:07 ..
drwxr-xr-x. 3 apache root 23 Nov 29 07:01 html
```


## 4. Test


- Windows : `c:\windows\system32\drivers\etc\hosts`

---

🌞 **Modifiez le fichier `hosts` de votre PC**

- ajoutez la ligne : `10.5.1.11 web.tp5.linux`
```powershell
PS C:\> echo 10.5.1.11 http://web.tp5.linux >> C:\Windows\System32\Drivers\Etc\Hosts
```

🌞 **Tester l'accès à NextCloud et finaliser son install'**

- ouvrez votre navigateur Web sur votre PC
- rdv à l'URL `http://web.tp5.linux`
```bash
[ced@web ~]$ curl http://web.tp5.linux
<!DOCTYPE html>
<html class="ng-csp" data-placeholder-focus="false" lang="en" data-locale="en" >
        <head>


 <title>
                Nextcloud               </title>
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
                                <meta name="apple-itunes-app" content="app-id=1125420102">
                                <meta name="theme-color" content="#0082c9">

# Le code n'est pas mis en entier par soucis de non-lourdeur
```