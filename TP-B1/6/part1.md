# Partie 1 : Préparation de la machine `backup.tp6.linux`

> On ne travaille que sur `backup.tp6.linux` dans cette partie !

# I. Ajout de disque

🌞 **Ajouter un disque dur de 5Go à la VM `backup.tp6.linux`**

```bash
[ced@backup ~]$ lsblk | grep sdb
sdb           8:16   0    5G  0 disk
```

# II. Partitioning

🌞 **Partitionner le disque à l'aide de LVM**

- créer un *physical volume (PV)* : le nouveau disque ajouté à la VM

```bash
[ced@backup ~]$ sudo pvcreate /dev/sdb
  Physical volume "/dev/sdb" successfully created.

[ced@backup ~]$ sudo pvs
  PV         VG Fmt  Attr PSize  PFree
  /dev/sda2  rl lvm2 a--  <7.00g    0
  /dev/sdb      lvm2 ---   5.00g 5.00g

[ced@backup ~]$ sudo pvdisplay
"/dev/sdb" is a new physical volume of "5.00 GiB"
  --- NEW Physical volume ---
  PV Name               /dev/sdb
  VG Name
  PV Size               5.00 GiB
  Allocatable           NO
  PE Size               0
  Total PE              0
  Free PE               0
  Allocated PE          0
  PV UUID               2RsrIP-geP6-BYsx-3twR-NcE6-OLTu-9a44tp
```

- créer un nouveau *volume group (VG)*

```bash
[ced@backup ~]$ sudo vgcreate backup /dev/sdb
  Volume group "backup" successfully created

[ced@backup ~]$ sudo vgs
  VG     #PV #LV #SN Attr   VSize  VFree
  backup   1   0   0 wz--n- <5.00g <5.00g
  rl       1   2   0 wz--n- <7.00g     0

[ced@backup ~]$ sudo vgdisplay
  --- Volume group ---
  VG Name               backup
  System ID
  Format                lvm2
  Metadata Areas        1
  Metadata Sequence No  1
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                0
  Open LV               0
  Max PV                0
  Cur PV                1
  Act PV                1
  VG Size               <5.00 GiB
  PE Size               4.00 MiB
  Total PE              1279
  Alloc PE / Size       0 / 0
  Free  PE / Size       1279 / <5.00 GiB
  VG UUID               B0e2OK-U6Pw-HF4L-jDKR-RPyG-iuUx-crockl
```

- créer un nouveau *logical volume (LV)* : ce sera la partition utilisable
```bash
[ced@backup ~]$ sudo lvcreate -l 100%FREE backup -n backup_data
  Logical volume "backup_data" created.

[ced@backup ~]$ sudo lvs
  LV          VG     Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  backup_data backup -wi-a-----  <5.00g
  root        rl     -wi-ao----  <6.20g
  swap        rl     -wi-ao---- 820.00m


[ced@backup ~]$ sudo lvdisplay
  --- Logical volume ---
  LV Path                /dev/backup/backup_data
  LV Name                backup_data
  VG Name                backup
  LV UUID                7duB3L-SceJ-eJc5-fNGa-9RJz-8Mia-KSw4D7
  LV Write Access        read/write
  LV Creation host, time backup.tp6.linux, 2021-12-10 17:42:27 +0100
  LV Status              available
  # open                 0
  LV Size                <5.00 GiB
  Current LE             1279
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:2
```

🌞 **Formater la partition**

- vous formaterez la partition en ext4 (avec une commande `mkfs`)

```bash
[ced@backup ~]$  mkfs -t ext4 /dev/backup/backup_data
mke2fs 1.45.6 (20-Mar-2020)
Could not open /dev/backup/backup_data: Permission denied

[ced@backup ~]$ sudo !!
sudo  mkfs -t ext4 /dev/backup/backup_data
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 1309696 4k blocks and 327680 inodes
Filesystem UUID: dd13863a-236a-4f6e-87d5-4cc8c10460ed
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376, 294912, 819200, 884736

Allocating group tables: done
Writing inode tables: done
Creating journal (16384 blocks): done
Writing superblocks and filesystem accounting information: done
```

🌞 **Monter la partition**

- montage de la partition (avec la commande `mount`)
```bash
[ced@backup ~]$ sudo mkdir /backup

[ced@backup ~]$ sudo mount /dev/backup/backup_data /backup

[ced@backup ~]$ df -h | grep backup
/dev/mapper/backup-backup_data  4.9G   20M  4.6G   1% /backup
```

- définir un montage automatique de la partition (fichier `/etc/fstab`)
```bash
[ced@backup ~]$ sudo nano /etc/fstab

[ced@backup ~]$ cat /etc/fstab
/dev/mapper/rl-root     /                       xfs     defaults        0 0
UUID=00b65ca7-7af2-44f2-ab75-c7223b12f446 /boot                   xfs     defaults        0 0
/dev/mapper/rl-swap     none                    swap    defaults        0 0
/dev/backup/backup_data /backup                 ext4    defaults        0 0

[ced@backup ~]$ reboot

[ced@backup ~]$ df -h | grep backup
/dev/mapper/backup-backup_data  4.9G   20M  4.6G   1% /backup
```

---

Ok ! Za, z'est fait. On a un espace de stockage dédié pour nos sauvegardes.  
Passons à la suite, [la partie 2 : installer un serveur NFS](./part2.md).


