# Partie 2 : Setup du serveur NFS sur `backup.tp6.linux`


🌞 **Préparer les dossiers à partager**

- créez deux sous-dossiers dans l'espace de stockage dédié
- `/backup/web.tp6.linux/`

```bash
[ced@backup ~]$ sudo mkdir /backup/web.tp6.linux
[ced@backup ~]$ ls
lost+found  web.tp6.linux
```

- `/backup/db.tp6.linux/`
```bash
[ced@backup ~]$ sudo mkdir /backup/db.tp6.linux
[ced@backup ~]$ ls
db.tp6.linux  lost+found  web.tp6.linux
```

🌞 **Install du serveur NFS**

- installez le paquet `nfs-utils`
```bash
[ced@backup ~]$ sudo dnf install nfs-utils

Dependencies resolved.


Installed:
  gssproxy-0.8.0-19.el8.x86_64     keyutils-1.5.10-9.el8.x86_64  libverto-libevent-0.3.0-5.el8.x86_64
  nfs-utils-1:2.3.3-46.el8.x86_64  rpcbind-1.2.5-8.el8.x86_64

Complete!
```

🌞 **Conf du serveur NFS**

- fichier `/etc/idmapd.conf`

```bash
[ced@backup ~]$ sudo nano /etc/idmapd.conf
[ced@backup ~]$ cat /etc/idmapd.conf
[General]
#Verbosity = 0
# The following should be set to the local NFSv4 domain name
# The default is the host's DNS domain name.
Domain = tp6.linux
```

- fichier `/etc/exports`

```bash
[ced@backup ~]$ sudo nano /etc/exports
[ced@backup ~]$ sudo cat /etc/exports
/backup/web.tp6.linux/  10.5.1.11/24(rw,no_root_squash)
/backup/db.tp6.linux/   10.5.1.12/24(rw,no_root_squash)
```


Les machins entre parenthèses `(rw,no_root_squash)` sont les options de partage. **Vous expliquerez ce que signifient ces deux-là.**

> - rw spécifie que les répertoires partagés sont en lecture/écriture
  - no_root_squash spécifie que le root de la machine sur laquelle le répertoire est monté a les droits de root sur le répertoire

🌞 **Démarrez le service**

- le service s'appelle `nfs-server`

```bash
[ced@backup ~]$ sudo systemctl start nfs-server
```

- après l'avoir démarré, prouvez qu'il est actif

```bash
[ced@backup ~]$ systemctl status nfs-server
● nfs-server.service - NFS server and services
   Loaded: loaded (/usr/lib/systemd/system/nfs-server.service; disabled; vendor preset: disabled)
   Active: active (exited) since Sun 2021-12-05 13:22:22 CET; 9s ago
  Process: 4910 ExecStart=/bin/sh -c if systemctl -q is-active gssproxy; then systemctl reload gssproxy>
  Process: 4898 ExecStart=/usr/sbin/rpc.nfsd (code=exited, status=0/SUCCESS)
  Process: 4897 ExecStartPre=/usr/sbin/exportfs -r (code=exited, status=0/SUCCESS)
 Main PID: 4910 (code=exited, status=0/SUCCESS)
```
- faites en sorte qu'il démarre automatiquement au démarrage de la machine

```bash
[ced@backup ~]$ sudo systemctl enable nfs-server
Created symlink /etc/systemd/system/multi-user.target.wants/nfs-server.service → /usr/lib/systemd/system/nfs-server.service.
```

🌞 **Firewall**

- le port à ouvrir et le `2049/tcp`
```bash
[ced@backup ~]$ sudo firewall-cmd --add-port=2049/tcp --permanent
success
```

- prouvez que la machine écoute sur ce port (commande `ss`)
```bash
[ced@backup ~]$ sudo ss -lptn | grep 2049
LISTEN 0      64           0.0.0.0:2049       0.0.0.0:*
LISTEN 0      64              [::]:2049          [::]:*
```

---

Ok le service est up & runnin ! Reste plus qu'à le tester : go sur [la partie 3 pour setup les clients NFS](./part3.md).