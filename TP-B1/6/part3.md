# Partie 3 : Setup des clients NFS : `web.tp6.linux` et `db.tp6.linux`


On commence par `web.tp6.linux`.

🌞 **Install'**

- le paquet à install pour obtenir un client NFS c'est le même que pour le serveur : `nfs-utils`

```bash
[ced@web ~]$ sudo dnf install nfs-utils
Installed:
  gssproxy-0.8.0-19.el8.x86_64     keyutils-1.5.10-9.el8.x86_64  libverto-libevent-0.3.0-5.el8.x86_64
  nfs-utils-1:2.3.3-46.el8.x86_64  rpcbind-1.2.5-8.el8.x86_64

Complete!
```

🌞 **Conf'**

- créez un dossier `/srv/backup` dans lequel sera accessible le dossier partagé
```bash
# création du dossier backup
[ced@web ~]$ sudo mkdir /srv/backup
```

- pareil que pour le serveur : fichier `/etc/idmapd.conf`

```bash
[ced@web ~]$ sudo nano /etc/idmapd.conf
[ced@web ~]$ cat /etc/idmapd.conf | grep domain
Domain = tp6.linux
```

---

🌞 **Montage !**

- montez la partition NFS `/backup/web.tp6.linux/` avec une comande `mount`
```bash
[ced@web ~]$ sudo mount -t nfs 10.5.1.13:/backup/web.tp6.linux/ /srv/backup
```

- la partition doit être montée sur le point de montage `/srv/backup`
```bash
[ced@web ~]$ df -h | grep back
10.5.1.13:/backup/web.tp6.linux  4.9G   20M  4.6G   1% /srv/backup
```

- preuve avec une commande `df -h` que la partition est bien montée
```bash
[ced@web ~]$ df -h | grep back
10.5.1.13:/backup/web.tp6.linux  4.9G   20M  4.6G   1% /srv/backup
```

- prouvez que vous pouvez lire et écrire des données sur cette partition
```bash
[ced@web ~]$ ls -l
total 0
drwxr-xr-x. 3 ced root 45 Dec  15 02:36 backup
drwxr-xr-x. 2 ced root 27 Nov 24 22:56 bin
```

- définir un montage automatique de la partition (fichier `/etc/fstab`)
```bash
# modification du fichier
[roxanne@web ~]$ sudo nano /etc/fstab
[roxanne@web ~]$ cat /etc/fstab

# /etc/fstab
/dev/mapper/rl-root     /                       xfs     defaults        0 0
UUID=8b3feda6-3fb4-4087-9e4a-e35644fee3ec /boot                   xfs     defaults        0 0
/dev/mapper/rl-swap     none                    swap    defaults        0 0
10.5.1.13:/backup/web.tp6.linux/        /srv/backup             nfs     defaults        0 0
```

- vous vérifierez que votre fichier `/etc/fstab` fonctionne correctement
```bash
[ced@web ~]$ sudo umount 10.5.1.13:/backup/web.tp6.linux/
[ced@web ~]$ sudo mount -av
/                        : ignored
/boot                    : already mounted
none                     : ignored
/srv/backup              : successfully mounted
```

---

🌞 **Répétez les opérations sur `db.tp6.linux`**


```bash
[ced@db ~]$ sudo dnf install nfs-utils
Dependencies resolved.

Complete!
```

```bash
[ced@db ~]$ sudo mkdir /srv/backup
[ced@db ~]$ ls /srv
backup
```

```bash
[ced@db ~]$ sudo nano /etc/idmapd.conf
[ced@db ~]$ cat /etc/idmapd.conf
[General]
#Verbosity = 0
# The following should be set to the local NFSv4 domain name
# The default is the host's DNS domain name.
Domain = tp6.linux
```

```bash
[ced@db ~]$ df -h | grep back
10.5.1.13:/backup/db.tp6.linux  4.9G   20M  4.6G   1% /srv/backup
```

```bash
[ced@db ~]$ sudo mount -av
/                        : ignored
/boot                    : already mounted
none                     : ignoredcd 
/srv/backup              : successfully mounted
```
---

Final step : [mettre en place la sauvegarde, c'est la partie 4](./part4.md).