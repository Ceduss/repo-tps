# TP 2

## 2. Nommer la machine


🌞 **Changer le nom de la machine**

#

```bash
ced@ced-vm:~$ sudo hostname node1.tp2.linux
# déco/reco
ced@node1:~$ sudo nano /etc/hostname
ced@node1:~$ cat /etc/hostname
node1.tp2.linux
```



## 3. Config réseau

🌞 **Config réseau fonctionnelle**


```bash
ced@node1:~$ ping 1.1.1.1
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from 1.1.1.1: icmp_seq=1 ttl=54 time=29.9 ms
```

```bash
ced@node1:~$ ping ynov.com
PING ynov.com (92.243.16.143) 56(84) bytes of data.
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=1 ttl=50 time=23.9 ms
```

```bash
ced@node1:~$ ping 192.168.239.10

Réponse de 192.168.239.10 : octets=32 temps<1ms TTL=64

Statistiques Ping pour 192.168.239.10:
    Paquets : envoyés = 3, reçus = 3, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 0ms, Maximum = 0ms, Moyenne = 0ms
```

# Go next


# Partie 1 : SSH


## 1. Installation du serveur


🌞 **Installer le paquet `openssh-server`**

```bash
ced@node1 sudo apt install openssh-server
Reading package lists... Done
Building dependency free
Reading state information... Done 
openssh-server is already the newest version (1:8.2p1-4ubuntu0.3).
0 upgraded, 0 newly installed, 0 to remove and 89 not upgraded.
```

## 2. Lancement du service SSH

🌞 **Lancer le service `ssh`**

```bash
ced@node1:~$ systemctl start sshd
```

```bash 
sced@node1:~$ ystemctl status
node1.tp2.linux
State: running
```

## 3. Etude du service SSH


🌞 **Analyser le service en cours de fonctionnement**

- afficher le statut du *service*
```bash
ced@node1:~$ systemctl status
● node1.tp2.linux
    State: running
```
- afficher le/les processus liés au *service* `ssh`
```bash
ced@node1:~$ ps -e | grep ssh
    859 ?        00:00:00 ssh-agent
   1556 ?        00:00:00 sshd
   1589 ?        00:00:00 sshd
   1669 ?        00:00:00 sshd
```

- afficher le port utilisé par le *service* `ssh`
```bash
ss -l | grep ssh
u_str   LISTEN   0        4096             /run/user/1000/gnupg/S.gpg-agent.ssh  25680                                           * 0
u_str   LISTEN   0        10                         /run/user/1000/keyring/ssh 26242                                           * 0
u_str   LISTEN   0        128                   /tmp/ssh-CjKNP8iVpONj/agent.776 25967                                           * 0
tcp     LISTEN   0        128                                           0.0.0.0:ssh                                       0.0.0.0:*
tcp     LISTEN   0        128                                              [::]:ssh                                          [::]:*
```
- afficher les logs du *service* `ssh`
```bash
ced@node1:~$ journalctl | grep ssh
oct. 19 16:38:06 ced-vm systemd[691]: Listening on GnuPG cryptographic agent (ssh-agent emulation).
oct. 19 16:38:19 ced-vm systemd[772]: Listening on GnuPG cryptographic agent (ssh-agent emulation).
```

🌞 **Connectez vous au serveur**

```bash
\Users\vidal>ssh ced@192.168.239.10
Welcome to Ubuntu 20.04.3 LTS (GNU/Linux 5.11.0-38-generic x86_64)

ced@node1:~$ 
```

## 4. Modification de la configuration du serveur


🌞 **Modifier le comportement du service**



- changer le ***port d'écoute*** du service *SSH*
```bash
ced@node1:~$ sudo nano /etc/ssh/sshd_config
# mettre le port sur 3000

ced@node1:~$ cat /etc/ssh/sshd_config
#Port 3000
#AddressFamily any
#ListenAddress 0.0.0.0
#ListenAddress ::
```

```bash
ced@node1:~$ sudo ss -alnpt
LISTEN      0           128                     0.0.0.0:3000                  0.0.0.0:*          users:(("sshd",pid=1602,fd=3))
LISTEN      0           128                        [::]:3000                     [::]:*          users:(("sshd",pid=1602,fd=4))
```

🌞 **Connectez vous sur le nouveau port choisi**

```bash
C:\Users\vidal>ssh -p 3000 ced@192.168.239.10

ced@node1:~$
```

# Partie 2 : FTP


# II. Setup du serveur FTP

## 1. Installation du serveur

🌞 **Installer le paquet `vsftpd`**

```bash
ced@node1:~$ sudo apt install vsftpd
Reading package lists... Done
Building dependency tree
Reading state information... Done
vsftpd is already the newest version (3.0.3-12).
0 upgraded, 1 newly installed, 0 to remove and 89 not upgraded.
```
```bash
ced@node1:~$ sudo cat /etc/vsftpd.conf
# Example config file /etc/vsftpd.conf
#
```

## 2. Lancement du service FTP

🌞 **Lancer le service `vsftpd`**

```bash
ced@node1:~$ systemctl start vsftpd

ced@node1:~$ systemctl status vsftpd
● vsftpd.service - vsftpd FTP server
     Loaded: loaded (/lib/systemd/system/vsftpd.service; enabled; vendor preset: enabled)
     Active: active (running) since Fri 2021-11-05 21:24:41 CET; 19h ago
```

## 3. Etude du service FTP

🌞 **Analyser le service en cours de fonctionnement**

- afficher le statut du service
```bash
ced@node1:~$ systemctl status vsftpd
● vsftpd.service - vsftpd FTP server
     Loaded: loaded (/lib/systemd/system/vsftpd.service; enabled; vendor preset: enabled)
     Active: active (running) since Fri 2021-11-05 21:24:41 CET; 19h ago
```

- afficher le/les processus liés au service `vsftpd`
```bash
ced@node1:~$ ps -e | grep vsftpd
   2221 ?       00:00:00 vsftpd
```

- afficher le port utilisé par le service `vsftpd`
```bash
ced@node1:~$ ss -l | grep ftp
tcp     LISTEN   0        32                                                  *:ftp                                             *:*

ced@node1:~$grep ftp /etc/services
ftp		21/tcp
```

- afficher les logs du service `vsftpd`
```bash
ced@node1:~$ journalctl | grep  vsftpd
nov. 05 21:24:37 node1.tp2.linux sudo[2004]:      ced : TTY=pts/1 ; PWD=/home/ced ; USER=root ; COMMAND=/usr/bin/apt install vsftpd
```

🌞 **Connectez vous au serveur**

- depuis votre PC, en utilisant un *client FTP*
```bash
ced@node1:~$ sudo cat /var/log/vsftpd.log
Sun Nov  7 15:17:05 2021 [pid 2966] CONNECT: Client "::ffff:192.168.239.1"
Sun Nov  7 15:17:21 2021 [pid 2965] [ced] OK LOGIN: Client "::ffff:192.168.239.1"
```

- essayez d'uploader et de télécharger un fichier
```bash
ced@node1:~$ sudo cat /var/log/vsftpd.log
Sun Nov  7 16:32:47 2021 [pid 4798] [ced] OK UPLOAD: Client "::ffff:192.168.239.1", "/home/ced/Documents/FTPTEST.txt", 24 bytes, 14.89Kbyte/sec
Sun Nov  7 16:33:44 2021 [pid 4804] [ced] OK DOWNLOAD: Client "::ffff:192.168.239.1", "/home/ced/Documents/FTPTEST.txt", 24 bytes, 44.56Kbyte/sec
```

- vérifier que l'upload fonctionne
```bash
ced@node1:~/Documents$ ls
FTPTEST.txt
```
🌞 **Visualiser les logs**

- mettez en évidence une ligne de log pour un download
```bash 
Sun Nov  7 16:32:47 2021 [pid 4798] [ced] OK UPLOAD: Client "::ffff:192.168.239.1", "/home/ced/Documents/FTPTEST.txt", 24 bytes, 14.89Kbyte/sec
```
- mettez en évidence une ligne de log pour un upload
```bash
Sun Nov  7 16:33:44 2021 [pid 4804] [ced] OK DOWNLOAD: Client "::ffff:192.168.239.1", "/home/ced/Documents/FTPTEST.txt", 24 bytes, 44.56Kbyte/sec
```

## 4. Modification de la configuration du serveur


🌞 **Modifier le comportement du service**

```bash
ced@node1:~$ sudo nano /etc/vsftpd.conf

ced@node1:~$ sudo cat /etc/vsftpd.conf
listen_port=3001

ced@node1:~$ systemctl restart vsftpd

ced@node1:~$ systemctl status vsftpd
● vsftpd.service - vsftpd FTP server
     Loaded: loaded (/lib/systemd/system/vsftpd.service; enabled; vendor preset: enabled)
     Active: active (running)

ced@node1:~$ ss -l | grep 3001
tcp     LISTEN   0        32                                                  *:3001                                            *:*                
```


🌞 **Connectez vous sur le nouveau port choisi**

```bash
Mon Nov  8 12:05:56 2021 [pid 6011] [ced] OK UPLOAD: Client "::ffff:192.168.239.1", "/home/ced/Documents/FTPTEST.txt", 24 bytes, 14.81Kbyte/sec

Mon Nov  8 12:06:03 2021 [pid 6014] [ced] OK DOWNLOAD: Client "::ffff:192.168.239.1", "/home/ced/Documents/FTPTEST.txt", 24 bytes, 57.59Kbyte/sec
```
```bash
ced@node1:~/Documents$ ls
FTPTEST.txt
```

# Partie 3 : Création de votre propre service
# II. Jouer avec netcat

S

🌞 **Donnez les deux commandes pour établir ce petit chat avec `netcat`**

- la commande tapée sur la VM
```bash
ced@node1:~$ nc -l 3002
```
- la commande tapée sur votre PC
```bash
ced@ced-vm:~$ echo yoyo | nc 192.168.239.10 3002
```

```bash
ced@node1:~$ nc -l 3002

ced@ced-vm:~$ echo yoyo | nc 192.168.239.10 3002

ced@node1:~$ nc -l 3002
yoyo
oyoy

ced@ced-vm:~$ echo yoyo | nc 192.168.239.10 3002
oyoy
```

🌞 **Utiliser `netcat` pour stocker les données échangées dans un fichier**



```bash
ced@node1:~$ nc -l 3002 >> NETCAT

ced@ced-vm:~$ nc 192.168.239.10 3002
bruh

ced@node1:~$ cat NETCAT
bruh
```

# III. Un service basé sur netcat


## 1. Créer le service

🌞 **Créer un nouveau service**

```bash
ced@node1:~$ sudo nano /etc/systemd/system/chat_tp2.service
ced@node1:~$ sudo chmod 777 /etc/systemd/system/chat_tp2.service
ced@node1:$ cat /etc/systemd/system/chat_tp2.service
[Unit]
Description=Little chat service (TP2)

[Service]
ExecStart=/usr/bin/nc -l 3003

[Install]
WantedBy=multi-user.target

ced@node1:/etc/systemd/system$ sudo systemctl daemon-reload
```
## 2. Test test et retest

🌞 **Tester le nouveau service**

```bash
ced@node1:$ sudo systemctl start chat_tp2
ced@node1:$ sudo systemctl status chat_tp2

ced@node1:~$ systemctl status chat_tp2.service
● chat_tp2.service - Little chat service (TP2)
     Active: active (running) since Mon 2021-11-08 17:06:23 CET; 2min 49s ago
   
ced@node1:~$ ss -l | grep 3003
tcp     LISTEN   0        1                                             0.0.0.0:3003                                      0.0.0.0:*

ced@ced-vm:~$ nc 192.168.251.14 2345
bonsouar
'hey
yoyoyo

ced@node1:~$ journalctl -xe -u chat_tp2 -f
nov. 08 17:21:45 node1.tp2.linux systemd[1]: Started Little chat service (TP2).
nov. 08 17:22:03 node1.tp2.linux nc[6602]: bonsouar
nov. 08 17:22:09 node1.tp2.linux nc[6602]: 'hey
nov. 08 17:22:13 node1.tp2.linux nc[6602]: yoyoyo
nov. 08 17:22:15 node1.tp2.linux systemd[1]: chat_tp2.service: Succeeded.
```
