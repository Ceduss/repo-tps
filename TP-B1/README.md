# TP

- [TP1 : Are you dead yet ?](./TP/1/README.md)
- [TP2 : Manipulation de services](./TP/2/README.md)
- [TP3 : Scripting](./TP/3/README.md)
- [TP4 : Une distribution orientée serveur](./TP/4/README.md)
- [TP5 : P'tit cloud perso](./TP/5/README.md)
- [TP6 : Stockage et sauvegarde](./TP/6/README.md)
