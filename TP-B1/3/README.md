# TP3 Linux


## Script carte d'identité

Ici le [script](https://gitlab.com/Ceduss/repo-tps/-/blob/main/TP/3/srv/idcard/idcard.sh) de l'idcard

### Exemple d'exécution d'une sortie : 

```shell=
Machine name: tp3.linux
OS: Ubuntu 20.04.3 LTS and the kernel version is 5.11.0-41-generic
IP: 127.0.1.1
RAM: 1,3Gi RAM restante sur 1,9Gi RAM totale
Disque : 1.8G space left
Top 5 processes by RAM usage :
/usr/lib/xorg/Xorg
xfwm4
xfdesktop
/usr/bin/python3
/usr/lib/x86_64-linux-gnu/xfce4/panel/plugins/libwhiskermenu.so

Listening ports: 
404 : systemd-r
442 : cupsd
442 : cupsd
535 : sshd
535 : sshd
556 : tor

Here's your random cat : https://cdn2.thecatapi.com/images/7f9.jpg
```
## Script Youtube-dl

Ici le [script](https://gitlab.com/Ceduss/repo-tps/-/blob/main/TP/3/srv/yt/yt.sh) du Youtube-dl

Ici les [Logs](https://gitlab.com/Ceduss/repo-tps/-/blob/main/TP/3/var/downloads.log) des téléchargements.


## MAKE IT A SERVICE

Iznogoud sowwy UwU